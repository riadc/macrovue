# MacroVue

## Installation

### 1. virtualenv / virtualenvwrapper
You should already know what is [virtualenv](http://www.virtualenv.org/), preferably [virtualenvwrapper](http://www.doughellmann.com/projects/virtualenvwrapper/) at this stage. So, simply run following command:

`$ mkvirtualenv --clear macrovue`

### 2. Download
Now, you need the *sandbox* project files in your workspace:

    $ cd /path/to/your/workspace
    $ git clone git@bitbucket.org:anilgalve/macrovue.git macrovue && cd macrovue

### 3. Requirements
Right there, you will find the *requirements.txt* file that has all the great debugging tools, django helpers and some other cool stuff. To install them, simply type:

`$ pip install -r requirements.txt`

If it gives error `Error loading MySQLdb module: No module named MySQLdb`
Then for ubuntu, `sudo apt-get install libmysqlclient-dev` & `sudo apt-get install python-dev`

#### Initialize the database
Remember to install necessary database driver for your engine and define your credentials in "macrovue/settings.py". Time to finish it up:

`./manage.py syncdb` and `./manage.py migrate`

### Ready? Go!

`./manage.py runserver`
