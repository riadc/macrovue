				{% for v in vues %}
					<tr>
						<td>
							<span class="vue-image">{{v.img}}</span>
							<h3 class="vue-name">{{v.name}}</h3>
							<p class="created">Created by: <a href="#">{{v.author}}</a></p>
						</td>
						<td>
							{{v.createdDate}}
						</td>
						<td>
							<span class=""></span>
						</td>	
						<td>
							<span class=""></span>
						</td>	
						<td><img src="{% static "images/macrovue2/growth-"{{v.growth}}".png" %}" /></td>
						<td><img src="{% static "images/macrovue2/valuation-"{{v.valuation}}".png" %}" /></td>
						<td><img src="{% static "images/macrovue2/risk-"{{v.risk}}".png" %}" /></td>
						<td><a href="#"><i class="fa fa-times"></i></a></td>
					</tr>
				{% endfor %}