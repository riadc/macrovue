# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SubscribedUser'
        db.create_table('subscribed_users', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'home', ['SubscribedUser'])

        # Adding model 'Categories'
        db.create_table('faq_categories', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'home', ['Categories'])

        # Adding model 'Questions'
        db.create_table('faq_questions', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('answer', self.gf('django.db.models.fields.CharField')(max_length=3000)),
        ))
        db.send_create_signal(u'home', ['Questions'])

        # Adding model 'CategoryQuestions'
        db.create_table(u'home_categoryquestions', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['home.Categories'])),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['home.Questions'])),
        ))
        db.send_create_signal(u'home', ['CategoryQuestions'])


    def backwards(self, orm):
        # Deleting model 'SubscribedUser'
        db.delete_table('subscribed_users')

        # Deleting model 'Categories'
        db.delete_table('faq_categories')

        # Deleting model 'Questions'
        db.delete_table('faq_questions')

        # Deleting model 'CategoryQuestions'
        db.delete_table(u'home_categoryquestions')


    models = {
        u'home.categories': {
            'Meta': {'object_name': 'Categories', 'db_table': "'faq_categories'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'home.categoryquestions': {
            'Meta': {'object_name': 'CategoryQuestions'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['home.Categories']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['home.Questions']"})
        },
        u'home.questions': {
            'Meta': {'object_name': 'Questions', 'db_table': "'faq_questions'"},
            'answer': ('django.db.models.fields.CharField', [], {'max_length': '3000'}),
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['home.Categories']", 'through': u"orm['home.CategoryQuestions']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'home.subscribeduser': {
            'Meta': {'object_name': 'SubscribedUser', 'db_table': "'subscribed_users'"},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['home']