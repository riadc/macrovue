# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

  def forwards(self, orm):
    # Adding model 'SubscribedUser'
    db.create_table('subscribed_users', (
      (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
      ('email', self.gf('django.db.models.fields.CharField')(max_length=50)),
    ))
    db.send_create_signal(u'home', ['SubscribedUser'])


  def backwards(self, orm):
    # Deleting model 'SubscribedUser'
    db.delete_table('subscribed_users')


  models = {
    u'home.subscribeduser': {
      'Meta': {'object_name': 'SubscribedUser', 'db_table': "'subscribed_users'"},
      'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
      u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
    }
  }

  complete_apps = ['home']
