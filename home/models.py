from django.db import models

class SubscribedUser(models.Model):
  email = models.CharField(max_length=50)

  class Meta:
    db_table = "subscribed_users"

class Categories(models.Model):
	name = models.CharField(max_length = 200)

	class Meta:
		db_table = "faq_categories"
		
	def __str__(self):
		return str(self.name)
			
class Questions(models.Model):
	title = models.CharField(max_length = 200)
	answer = models.CharField(max_length = 3000)
	category = models.ManyToManyField(Categories, through='CategoryQuestions')
	class Meta:
		db_table = "faq_questions"
		
	def __str__(self):
		return str(self.title)
		
class CategoryQuestions(models.Model):
	category = models.ForeignKey(Categories)
	question = models.ForeignKey(Questions)