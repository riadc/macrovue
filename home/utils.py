from django.conf import settings
import mailchimp

def get_mailchimp_api():
  return mailchimp.Mailchimp(settings.MAILCHIMP_API_KEY)

def get_subscription_list_id(mailchimp_api):
  l = mailchimp_api.lists.list({ "list_name": settings.MAILCHIMP_LIST_NAME })
  return l['data'][0]['id']
