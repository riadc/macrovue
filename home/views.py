from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib import messages
from django.http import HttpResponse

from home.utils import get_mailchimp_api, get_subscription_list_id
from home.models import SubscribedUser, Questions, Categories, CategoryQuestions
import mailchimp

# the index of the website
def index(request): 
  return render_to_response('home/index.html', context_instance=RequestContext(request))

# subscribe to the website
def subscribe(request):
  try:
    m = get_mailchimp_api()
    list_id = get_subscription_list_id(m)
    email = request.POST['email']
    m.lists.subscribe(list_id, {'email':email})
    SubscribedUser.objects.get_or_create(email=email)
  except mailchimp.ListAlreadySubscribedError:
    return HttpResponse('That email is already subscribed to the list')
  except mailchimp.Error, e:
    return HttpResponse('An error occurred: %s - %s' % (e.__class__, e))
  return HttpResponse('')

def about(request):
  return render_to_response('home/about.html', locals(), context_instance=RequestContext(request))
  
def faq(request):
  breadcrumbs = {1 : 'Existing Vues' , 'url1' : 'existing_vues', 2 : 'FAQ', 'url2' : 'faq'}
  faq_questions = Questions.objects.all()
  faq_categories = Categories.objects.all()
  faq_catquestions = CategoryQuestions.objects.all()
  
  return render_to_response('home/faq.html', locals(), context_instance=RequestContext(request))