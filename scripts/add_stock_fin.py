import os
import csv
import MySQLdb
import time, datetime
db = MySQLdb.connect(host="mv-db.ck2w2jitsnqg.ap-southeast-2.rds.amazonaws.com",user="root",passwd="awiventures", db='mv_db')

cursor = db.cursor()

directory = "/Users/RAF/Box Sync/Data/StockFinData"
for filename in os.listdir(directory):
	if filename.endswith(".csv"):				
		date = filename.split("_")[1].replace('.csv','')
		dateentered = str(time.strptime(date, '%d-%B-%Y'))

		csv_data = csv.reader(file(directory+"/"+filename))
		for row in csv_data:
			#print row
			ciq_id = row[0]
			dividend_yield = row[1]
			last_sale_price = row[2]
			market_cap = row[3]
			price_equity = row[4]
			one_year_vol = row[5]
			one_month_return = row[6]
			one_year_return = row[7]
			one_month_last_sale_price = row[8]
			one_year_last_sale_price = row[9]
			
			cursor.execute("SELECT id FROM stocks WHERE ciq_id='"+ciq_id+"'")

			if (cursor.rowcount == 0):
				print "Did not enter data"
			else:
				stock_id = str(cursor.fetchone()[0])
				cursor.execute("INSERT INTO stock_financial_data(stock_id,date,dividend_yield,price_equity_ratio," +
				" mkt_cap,one_mo_return,one_yr_return,one_year_vol,last_sale_price,one_month_last_sale_price,one_year_last_sale_price)"+
				" VALUES("+stock_id+", '"+dateentered+"', '"+dividend_yield+"', '"+price_equity+"', '"+market_cap+"', '"+one_month_return+
				"', '"+one_year_return+"', '"+one_year_vol+"', '"+last_sale_price+"', '"+one_month_last_sale_price+"', '"+one_year_last_sale_price+"')")
				print "Entered data for " + stock_id

db.commit()
cursor.close()
db.close()

