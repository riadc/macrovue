import os
import csv
import MySQLdb
import time, datetime
db = MySQLdb.connect(host="mv-db.ck2w2jitsnqg.ap-southeast-2.rds.amazonaws.com",user="root",passwd="awiventures", db='mv_db')

cursor = db.cursor()

directory = "/Users/RAF/Box Sync/Data/VueFinData"
for filename in os.listdir(directory):
	if filename.endswith(".csv"):
		# grab the name without the .csv
		vue_name = filename.replace('.csv','') 
		# find the id in the db
		cursor.execute("SELECT id FROM vues WHERE name='"+vue_name+"'")
		# grab the row
		vue_id = str(cursor.fetchone()[0])
		cursor.execute("SELECT date FROM vue_financial_data WHERE vue_id="+vue_id+" ORDER BY id DESC")
		if cursor.rowcount > 0:
			last_date = datetime.datetime.strptime(str(cursor.fetchone()[0]), "%Y-%m-%d")
			prev = 1
		else:
			prev = 0
		csv_data = csv.reader(file(directory+"/"+filename))
		for row in csv_data:
			date = row[0]
			dividend_yield = row[1].replace("%","")
			valuation = row[2]
			stock_volatility = row[3]
			one_mo_return = row[4].replace("%","")
			if (prev == 0) or (last_date < datetime.datetime.strptime(date, "%Y-%m-%d")):
				cursor.execute("INSERT INTO vue_financial_data(vue_id,date,dividend_yield,valuation,stock_volatility,one_mo_return)"+
				" VALUES("+vue_id+", '"+date+"', "+dividend_yield+", "+valuation+", "+stock_volatility+", "+one_mo_return+")")
				print "Entered data for " + date

db.commit()
cursor.close()
db.close()