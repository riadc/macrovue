# Written by Riad C.

import os
import csv
import MySQLdb
import datetime
import pandas as pd
from pandas import *
from pandas.io import *
db = MySQLdb.connect(host="mv-db.ck2w2jitsnqg.ap-southeast-2.rds.amazonaws.com",user="root",passwd="awiventures", db='mv_db')

cursor = db.cursor()

directory = "/Users/RAF/Box Sync/Data/VueReturns"
for filename in os.listdir(directory):
	if filename.endswith(".csv"):
		data = pd.read_csv(directory+'/'+filename)
		df = DataFrame(data = data)
		for row in df.iterrows():
			if (str(row[1]["Date"]) == "nan"):
				continue
			date = datetime.strptime(row[1]["Date"], '%d-%b-%Y').strftime('%Y-%m-%d')
			row_data = row[1]
			i = 0;
			for vue_info in row_data:
				if i > 0:
					name = list(data.columns.values)[i]
					cursor.execute("SELECT id FROM vues WHERE name='"+name+"'")
					vue_id = str(cursor.fetchone()[0])
					print "Entered data for: " + str(vue_id) + " - " + str(date) + " - " + str(vue_info)
					cursor.execute("INSERT INTO vue_returns(vue_id,date,return_percentage) "+
					"VALUES('"+str(vue_id)+"','"+str(date)+"','"+str(vue_info)+"')")

				i=i+1
db.commit()
cursor.close()
db.close()