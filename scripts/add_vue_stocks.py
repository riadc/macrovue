import os
import csv
import MySQLdb
import time, datetime
import pandas as pd
db = MySQLdb.connect(host="mv-db.ck2w2jitsnqg.ap-southeast-2.rds.amazonaws.com",user="root",passwd="awiventures", db='mv_db')

cursor = db.cursor()

directory = "/Users/RAF/Box Sync/Data/VueStocks/"
for filename in os.listdir(directory):
	if filename.endswith(".csv"):
		# grab the row
		csv_data = csv.reader(file(directory+"/"+filename))
		i = 0;
		for row in csv_data:
			if (i > 0 and row[0] != ''):
				name = row[0]
				country = row[1]
				category_list = row[2]
				created_at = row[3]
				updated_at = row[4]
				stock_list = row[5]
			
				cursor.execute("SELECT * FROM vues WHERE name='"+name+"'")
				# does this vue already exist
				if cursor.rowcount > 0:
					print "This vue already exists. Let's see if it needs updates."
					# grab the row
					vue_info = cursor.fetchone()
					# we need the actual id
					vue_id = str(vue_info[0])
					if vue_info[9] != country:
						cursor.execute("UPDATE vues SET country='"+country+"' WHERE id="+str(vue_info[0]))
						print "Updated country of " +str(vue_info[1])+ " to " + country			
				else: #no it does not exist
					cursor.execute("INSERT INTO vues(name,user_generated,description,short_description,user_id,country,created_at,updated_at,background,is_visible) "+
					"VALUES('"+name+"',0,'n/a','n/a',1,'"+country+"','0000-00-00','0000-00-00','http','0')")
					db.commit()
					cursor.execute("SELECT id FROM vues WHERE name='"+name+"'")
					vue_id = str(cursor.fetchone()[0])
					cursor.execute("UPDATE vues SET created_at='"+created_at+"',updated_at='"+updated_at+"' WHERE id="+vue_id+")")
					db.commit()
					print "Created the vue for " + name + " - id: " + vue_id
			
				for category in category_list.split(" "):
					cursor.execute("SELECT id FROM menu WHERE name='"+category+"'")
					menu_id = str(cursor.fetchone()[0])	
					cursor.execute("SELECT id FROM vue_menus WHERE vue_id="+vue_id+" AND menu_id="+menu_id)
					if cursor.rowcount == 0:
						cursor.execute("INSERT INTO vue_menus(vue_id,menu_id) VALUES("+vue_id+", "+menu_id+")")
						print "Added " + vue_id + " to " + menu_id
	
				print "Finished updating"
				
				for stock in stock_list.split(" "):
					cursor.execute("SELECT id FROM stocks WHERE ticker='"+stock+"'")
					stock_id = str(cursor.fetchone()[0])
					cursor.execute("INSERT INTO vue_stocks(vue_id,stock_id) VALUES("+vue_id+", "+stock_id+")")
					print "Entered data for " + stock + " to vue " + vue_id
			
			i=i+1

db.commit()
cursor.close()
db.close()