$(document).ready(function(){
	$('#tab2-content').hide();
	// change the page tab
	$('.tab').on('click', function() {
		var id = this.id;
		$('.tab-content').hide();
		$('#'+id+'-content').show();
		$('.tab').removeClass('green_haze active-tab');
		$('#'+id).addClass('green_haze active-tab');
	});

});