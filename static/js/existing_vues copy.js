/*
* Build a vue using Macrovues quick system
* Track the stocks they want to start their vue with
* IN CREATE_VUE.HTML: Add to an input box an: id (vue_****), and the class: stocks_container_content
* Then: in the right container '#stocks_chosen_container', include a span with the id (above) + '_instocks'
*/

jQuery(document).ready(function($) {
	var currStocks = [];

	function add_stock(stock) {
		currStocks.push(stock);
		$('#stock_list').val(currStocks);
		return true;
	}
	
	function remove_stock(stock) {
		var position = $.inArray(stock, currStocks);
		currStocks.splice(position, 1);
		$('#stock_list').val(currStocks);
		return true;
	}
	
	$('.stocks_container_content').on('keyup', function() {
		if ($('#'+this.id).val() == '') {
			$('#'+this.id+'_instocks').text($('#'+this.id).attr('placeholder'));
			$('#'+this.id+'_instocks').css('background', '#FFF');
		} else {
			$('#'+this.id+'_instocks').text($('#'+this.id).val());
			$('#'+this.id+'_instocks').css('background', '#DDFCC7');
		}
	});
	
	$("#stock_options").on('click','.stock_option', function() {
		jQuery('#'+this.id).hide();
		jQuery('#'+this.id).appendTo('#stocks_chosen');
		jQuery('#'+this.id).show();
		add_stock(this.id.replace('stock',''));
	});
	
	$("#stocks_chosen").on('click','.stock_option', function() {
		jQuery('#'+this.id).hide();
		jQuery('#'+this.id).appendTo('#stock_options');
		remove_stock(this.id.replace('stock',''));		
	});
	
	$('#stock_search_submit').on('click', function() {
		console.log('true');
		submit_stock_search();
	});
	
	$('#vue_stock').on('keyup', function(e) {
		console.log('true');
		if (e.keyCode == 13)
			submit_stock_search();
	});
	
	function submit_stock_search() {
		var url = $('#vue_stock').val();
		if (url == '') {
			$('#stock_options').html('<p style="margin-left: 20px">Please enter a ticker to search.</p>');
			return false;
		}
		$.ajax({
			type:'GET',
			url: '/getstocks/'+url,
			async: false,
			dataType: "json",
			data:{},
			success: function(response) {
				$('#stock_options').hide();
				var output = '';
				for (var property in response) {				
					if (!$.inArray(response[property][0], currStocks)) {
						continue;
					}
					output += '<div class="stock_option" id="stock'+response[property][0]+'">'+response[property][1]+': '+response[property][3]+'</div>';
				}
				if (output == '') {
					output = '<p style="margin-left: 20px">No results, please try again.</p>';
				}
				$('#stock_options').html(output);
				$('#stock_options').fadeIn(400);
			}
		});
		return true;
	}
	
});