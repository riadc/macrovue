$(document).ready(function($) {
	/* 
	 * Sidebar functionality
	 */
	//giving each li element on the sidebar a unique id corresponding with category
	for(var i = 0; i <= $('.sidebar-list li').size(); i++){
		$('.sidebar-list li:nth-child('+ i +')').attr('id', i);
	}
	

	//Giving each category div an id
	for(var i = 0; i <= $('#content-container h2').size(); i++){
		$('#content-container .category:nth-of-type('+ i + ')').attr('id', i);
	}

	//when the sidebar is clicked ==> scroll to the category only
	$('.sidebar-list li').on('click', function(){
		var catID = $(this).attr('id');
		$('html, body').animate({
			scrollTop: $('#content-container #' + catID).offset().top - 100
		}, 1000);
	});

 
	 //faq question hiding and showing
	$('.faq_question').on('click', function() {
		var id = this.id;
		if ($('#faq'+id).is(':visible')) {
			$('#faq'+id).slideUp(400);
			$('#icon'+id).html('<i class="fa fa-angle-down"></i>');
			return false;
		}
//		$('.question_arrow').html('<i class="fa fa-angle-down"></i>');
//		$('.faq_answer').slideUp(400);
		$('#faq'+id).slideDown(400);
		$('#icon'+id).html('<i class="fa fa-angle-right"></i>');
		
		return false;
	});

});