jQuery(document).ready(function($) {
	$('.box_label').on('click', function() {
		var chart = $('#chartmain').highcharts();
		var name = this.id.replace('-line','');
		for (i = 0; i < names.length; i++) {
			if (name == names[i])
				break;
		}
        series = chart.series[i];
        
		if ($('#'+this.id+'-box').prop('checked')) {
			series.show();	
		} else
			series.hide();
	});

	var seriesOptions = [],
		yAxisOptions = [],
		seriesCounter = 0,
		names = [$('#vue_name').val(),$('#vue_index').val()],
		ids = [$('#vue_id').val(),$('#vue_id').val()+'-index']
		total = names.length,
		colors = Highcharts.getOptions().colors;

	$.each(names, function(i, name) {
		$.getJSON('/getvuereturns/'+ids[i],	function(data) {			
			if (i == 0) {
				var color = '#0d5c6b';
			} else if (i == 1) {
				var color = '#3FAFD1';
			}
							
			seriesOptions[i] = {
				name: name,
				data: data,
				color: color,
				_symbolIndex: i
			};
			// As we're loading the data asynchronously, we don't know what order it will arrive. So
			// we keep a counter and create the chart when all the data is loaded.
			seriesCounter++;
			if (seriesCounter == total)
			{
				createChart('main');
			}
		});
	});

	// create the chart when all data is loaded
	function createChart(name) {
		$('#chart'+name).highcharts('StockChart', {

		    rangeSelector: {
				inputEnabled: $('#chart'+name).width() > 480,
		        selected: 4
		    },

		    yAxis: {
		    	labels: {
		    		formatter: function() {
		    			return (this.value > 0 ? '+' : '') + this.value + '%';
		    		}
		    	},
		    	plotLines: [{
		    		value: 0,
		    		width: 2,
		    		color: 'silver'
		    	}]
		    },
		    
		    xAxis: {
		    	type: 'datetime'
		    },

		    plotOptions: {
		    	series: {
		    		compare: 'value'
		    	}
		    },
		    
		    tooltip: {
		    	pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
		    	valueDecimals: 2
		    },
		    
		    series: seriesOptions
		});
	}
});
