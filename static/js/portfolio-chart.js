$(document).ready(function(){
    var wk = 0;
    var omo = 0;
    var tmo = 0;
    var yr = 0;
    var ytd = 0;
    var fin = 0;
    //portfolio - all 
    $.getJSON('/getportfolio/0', function (data){

        $('#chartdollar-all').highcharts({
            chart: {
                type: 'area'
            }, 

            title: { 
                text: ''
            },
            legend: {
                enabled: false
            },
            xAxis: {
                type: 'datetime'
            },

            yAxis: {
                title: {
                    text: 'Portfolio Value'
                },  
                labels: {
                    formatter: function() {
                        return (this.value == 0 ? ' ' : (this.value > 0 ? '+' : '-')) + '$' + (this.value < 0 ? (Math.abs(this.value / 1000)) : (this.value/1000 )) + ( (this.value/ 1000 > 1) ? 'k' : '');
                    }
                }   
            },

            series : [{
                name : 'Total Value',
                data : data,
                color: '#6BC396',
                tooltip : {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>${point.y}</b><br/>',
                    valueDecimals: 2
                }
            }]
        });
    });

    //all - percent 
    $.getJSON('/getportfolio_percent/0', function (data){

        $('#chartpercent-all').highcharts({
            chart: {
                type: 'area'
            }, 

            title: { 
                text: ''
            },
            legend: {
                enabled: false
            },
            rangeSelector: {
                enabled: false
            },
            
            xAxis: {
                type: 'datetime'
            },

            yAxis: {
                title: {
                    text: 'Portfolio Value'
                }, 
                labels: {
                    formatter: function() {
                        return (this.value > 0 ? '+' : '') + this.value + '%'
                    }
                }   
            },

            series : [{
                name : 'Total Value',
                data : data,
                color: '#6BC396',
                tooltip : {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                    valueDecimals: 2
                }
            }]
        });
    });
    ////////////
    // 1 week //
    ////////////
    $.getJSON('/getportfolio/1', function (data){
        if(data.length < 1){
            $('#wk-btn').addClass('faded');
        } else {
            $('#chartdollar-wk').highcharts({
                chart: {
                    type: 'area'
                }, 

                title: { 
                    text: ''
                },
                legend: {
                    enabled: false
                },              
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Portfolio Value'
                    },
                    labels: {
                        formatter: function() {
                            return (this.value == 0 ? ' ' : (this.value > 0 ? '+' : '-')) + '$' + (this.value < 0 ? (Math.abs(this.value / 1000)) : (this.value/1000 )) + ( (this.value/ 1000 > 1) ? 'k' : '');
                        }
                    }   
                },
                series : [{
                    name : 'Total Value',
                    data : data,
                    color: '#6BC396',
                    tooltip : {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>${point.y}</b><br/>',
                        valueDecimals: 2
                    }
                }]
            });            
        }
    });   

    $.getJSON('/getportfolio_percent/1', function (data){
        $('#chartpercent-wk').highcharts({
            chart: {
                type: 'area'
             }, 
             title: { 
                 text: ''
             },
             legend: {
                 enabled: false
             },
             rangeSelector : {
                 enabled: false
             },
             xAxis: {
                 type: 'datetime'
             },
             yAxis: {
                title: {
                    text: 'Portfolio Value'
                },
                 labels: {
                     formatter: function() {
                         return (this.value > 0 ? '+' : '') + this.value + '%'
                     }
                 }   
             },
             series : [{
                 name : 'Total Value',
                 data : data,
                 color: '#6BC396',
                 tooltip : {
                     pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                     valueDecimals: 2
                 }
             }]
        });
    });

    ////////////
    //1 month //
    ////////////
    $.getJSON('/getportfolio/2', function (data){
        if(data.length < 1){
            $('#omo-btn').addClass('faded');
        } else {
            $('#chartdollar-omo').highcharts({
                chart: {
                    type: 'area'
                }, 

                title: { 
                    text: ''
                },
                legend: {
                    enabled: false
                },
                rangeSelector : {
                    enabled: false
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Portfolio Value'
                    },
                    labels: {
                        formatter: function() {
                            return (this.value == 0 ? ' ' : (this.value > 0 ? '+' : '-')) + '$' + (this.value < 0 ? (Math.abs(this.value / 1000)) : (this.value/1000 )) + ( (this.value/ 1000 > 1) ? 'k' : '');
                        }
                    }   
                },
                series : [{
                    name : 'Total Value',
                    data : data,
                    color: '#6BC396',
                    tooltip : {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>${point.y}</b><br/>',
                        valueDecimals: 2
                    }
                }]
            });

            
        }
    });

    $.getJSON('/getportfolio_percent/2', function (data){
            $('#chartpercent-omo').highcharts({
                chart: {
                    type: 'area'
                }, 

                title: { 
                    text: ''
                },
                legend: {
                    enabled: false
                },
                rangeSelector : {
                    enabled: false
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Portfolio Value'
                    },
                    labels: {
                        formatter: function() {
                            return (this.value > 0 ? '+' : '') + this.value + '%'
                        }
                    }   
                },
                series : [{
                    name : 'Total Value',
                    data : data,
                    color: '#6BC396',
                    tooltip : {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                        valueDecimals: 2
                    }
                }]
            });
    });

    //3 months
    $.getJSON('/getportfolio/3', function (data){
        if(data.length < 1){
            $('#tmo-btn').addClass('faded');
        } else {
            $('#chartdollar-tmo').highcharts({
                chart: {
                    type: 'area'
                }, 

                title: { 
                    text: ''
                },
                legend: {
                    enabled: false
                },
                rangeSelector : {
                    enabled: false
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Portfolio Value'
                    },
                    labels: {
                        formatter: function() {
                            return (this.value == 0 ? ' ' : (this.value > 0 ? '+' : '-')) + '$' + (this.value < 0 ? (Math.abs(this.value / 1000)) : (this.value/1000 )) + ( (this.value/ 1000 > 1) ? 'k' : '');
                        }
                    }   
                },
                series : [{
                    name : 'Total Value',
                    data : data,
                    color: '#6BC396',
                    tooltip : {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>${point.y}</b><br/>',
                        valueDecimals: 2
                    }
                }]
            });
        }
    });
    
    $.getJSON('/getportfolio_percent/3', function  (data){
            $('#chartpercent-tmo').highcharts({
                chart: {
                    type: 'area'
                }, 

                title: { 
                    text: ''
                },
                legend: {
                    enabled: false
                },
                rangeSelector : {
                    enabled: false
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Portfolio Value'
                    },
                    labels: {
                        formatter: function() {
                            return (this.value > 0 ? '+' : '') + this.value + '%'
                        }
                    }   
                },
                series : [{
                    name : 'Total Value',
                    data : data,
                    color: '#6BC396',
                    tooltip : {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                        valueDecimals: 2
                    }
                }]
            });
    });

    //1 year
    $.getJSON('/getportfolio/4', function (data){
        if(data.length < 1){
            $('#yr-btn').addClass('faded');
        } else {
            $('#chartdollar-yr').highcharts({
                chart: {
                    type: 'area'
                }, 
                legend: {
                    enabled: false
                },
                title: { 
                    text: ''
                },
                rangeSelector : {
                    enabled: false
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Portfolio Value'
                    },
                    labels: {
                        formatter: function() {
                            return (this.value == 0 ? ' ' : (this.value > 0 ? '+' : '-')) + '$' + (this.value < 0 ? (Math.abs(this.value / 1000)) : (this.value/1000 )) + ( (this.value/ 1000 > 1) ? 'k' : '');
                        }
                    }   
                },
                series : [{
                    name : 'Total Value',
                    data : data,
                    color: '#6BC396',
                    tooltip : {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>${point.y}</b><br/>',
                        valueDecimals: 2
                    }
                }]
            });            
        }
    });

    $.getJSON('/getportfolio_percent/4', function  (data) {
                    $('#chartpercent-yr').highcharts({
                chart: {
                    type: 'area'
                }, 

                title: { 
                    text: ''
                },
                legend: {
                    enabled: false
                },
                rangeSelector : {
                    enabled: false
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Portfolio Value'
                    },
                    labels: {
                        formatter: function() {
                            return (this.value > 0 ? '+' : '') + this.value + '%'
                        }
                    }   
                },
                series : [{
                    name : 'Total Value',
                    data : data,
                    color: '#6BC396',
                    tooltip : {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                        valueDecimals: 2
                    }
                }]
            });
    });
    
    //year to date
    $.getJSON('/getportfolio/5', function (data){
        if(data.length < 1){
            $('#ytd-btn').addClass('faded');
        } else {
            $('#chartdollar-ytd').highcharts({
                chart: {
                    type: 'area'
                }, 

                title: { 
                    text: ''
                },
                legend: {
                    enabled: false
                },
                rangeSelector : {
                    enabled: false
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Portfolio Value'
                    },
                    labels: {
                        formatter: function() {
                            return (this.value == 0 ? ' ' : (this.value > 0 ? '+' : '-')) + '$' + (this.value < 0 ? (Math.abs(this.value / 1000)) : (this.value/1000 )) + ( (this.value/ 1000 > 1) ? 'k' : '');
                        }
                    }   
                },
                series : [{
                    name : 'Total Value',
                    data : data,
                    color: '#6BC396',
                    tooltip : {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>${point.y}</b><br/>',
                        valueDecimals: 2
                    }
                }]
            });
                       
        }
    });

    $.getJSON('/getportfolio_percent/5', function (data){
        $('#chartpercent-ytd').highcharts({
                chart: {
                    type: 'area'
                }, 

                title: { 
                    text: ''
                },
                legend: {
                    enabled: false
                },
                rangeSelector : {
                    enabled: false
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Portfolio Value'
                    },
                    labels: {
                        formatter: function() {
                            return (this.value > 0 ? '+' : '') + this.value + '%'
                        }
                    }   
                },
                series : [{
                    name : 'Total Value',
                    data : data,
                    color: '#6BC396',
                    tooltip : {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                        valueDecimals: 2
                    }
                }]
            });
    });

    //financial year
    $.getJSON('/getportfolio/6', function (data){
        if(data.length < 1){
            $('#fin-btn').addClass('faded');
        } else {
            $('#chartdollar-fin').highcharts({
                chart: {
                    type: 'area'
                }, 

                title: { 
                    text: ''
                },
                legend: {
                    enabled: false
                },
                rangeSelector : {
                    enabled: false
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Portfolio Value'
                    },
                    labels: {
                        formatter: function() {
                            return (this.value == 0 ? ' ' : (this.value > 0 ? '+' : '-')) + '$' + (this.value < 0 ? (Math.abs(this.value / 1000)) : (this.value/1000 )) + ( (this.value/ 1000 > 1) ? 'k' : '');
                        }
                    }   
                },
                series : [{
                    name : 'Total Value',
                    data : data,
                    color: '#6BC396',
                    tooltip : {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>${point.y}</b><br/>',
                        valueDecimals: 2
                    }
                }]
            });            
        }
    });

    $.getJSON('/getportfolio_percent/6', function (data){
            $('#chartpercent-fin').highcharts({
                chart: {
                    type: 'area'
                }, 

                title: { 
                    text: ''
                },
                legend: {
                    enabled: false
                },
                rangeSelector : {
                    enabled: false
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Portfolio Value'
                    },
                    labels: {
                        formatter: function() {
                            return (this.value > 0 ? '+' : '') + this.value + '%'
                        }
                    }   
                },
                series : [{
                    name : 'Total Value',
                    data : data,
                    color: '#6BC396',
                    tooltip : {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                        valueDecimals: 2
                    }
                }]
            });
    });
});