$(document).ready(function(){
	$('#tab2-content').hide();
	$('#tab3-content').hide();


	/* 
	 * Charts 
	 */

	$('#country-chart').highcharts({
		    chart: {
		        renderTo: 'container',
		        type: 'pie',
		    },
		    title: {
		    	text: ''
		    },
		    tooltip: {
		        formatter: function() {
		            return this.point.name + " (" + this.percentage + "%)";
		        }
		    },
		    legend: {
		        labelFormatter: function() {
		            return this.name + " (" + this.percentage + "%)";
		        }
		    },
		    plotOptions: {
		        pie: {
		            allowPointSelect: true,
		            cursor: 'pointer',
		            dataLabels: {
		                enabled: false
		            },
		            showInLegend: true
		        }
		    },
		    series: [{
		        type: 'pie',
		        name: 'Browser share',
		        data: [
		            ['Firefox', 45.0],
		            ['IE', 26.8],
		            ['Chrome', 12.8],
		            ['Safari', 8.5],
		            ['Opera', 6.2],
		            ['Others', 0.7]
		            ]}]
		});

	$('#sector-chart').highcharts({
		    chart: {
		        renderTo: 'container',
		        type: 'pie',
		    },
		    title: {
		    	text: ''
		    },
		    tooltip: {
		        formatter: function() {
		            return this.point.name + " (" + this.percentage + "%)";
		        }
		    },
		    legend: {
		        labelFormatter: function() {
		            return this.name + " (" + this.percentage + "%)";
		        }
		    },
		    plotOptions: {
		        pie: {
		            allowPointSelect: true,
		            cursor: 'pointer',
		            dataLabels: {
		                enabled: false
		            },
		            showInLegend: true
		        }
		    },
		    series: [{
		        type: 'pie',
		        name: 'Browser share',
		        data: [
		            ['Firefox', 45.0],
		            ['IE', 26.8],
		            ['Chrome', 12.8],
		            ['Safari', 8.5],
		            ['Opera', 6.2],
		            ['Others', 0.7]
		            ]}]
		});





	// change the page tab
	$('.tab').on('click', function() {
		var id = this.id;
		$('.tab-content').hide();
		$('#'+id+'-content').show();
		$(window).resize();
		$('.tab').removeClass('green_haze active-tab');
		$('#'+id).addClass('green_haze active-tab');
	});

})