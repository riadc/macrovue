/*
* Build a vue using Macrovues quick system
* Track the stocks they want to start their vue with
* IN CREATE_VUE.HTML: Add to an input box an: id (vue_****), and the class: stocks_container_content
* Then: in the right container '#stocks_chosen_container', include a span with the id (above) + '_instocks'
*/
jQuery(document).ready(function($) {
	var currStocks = [];
	var appendedError = false;
	colourTitle(1);
	$('#vue_name').keyup(function(){
		$('.vue_preview_name').text($(this).val());
		$(this).trigger('focusout');
	}).focusout(function() {
		if($(this).val().length == 0){
			$('.vue_preview_name').addClass('preview');
			$('.vue_preview_name').text("Vue Name");
		} else {
			$('.vue_preview_name').removeClass('preview');
		}
	});


	$('#vue_short_description').keyup(function(){
		$('.vue_preview_short').text($(this).val());
		$(this).trigger('focusout');
	}).focusout(function() {
		if($(this).val().length == 0){
			$('.vue_preview_short').addClass('preview');
			$('.vue_preview_short').text("Description");
		} else {
			$('.vue_preview_short').removeClass('preview');
		}
	});

	$('#vue_description').keyup(function(){
		$('.vue_preview_desc').text($(this).val());
		$(this).trigger('focusout');
	}).focusout(function() {
		if($(this).val().length == 0){
			$('.vue_preview_desc').addClass('preview');
			$('.vue_preview_desc').text("Description");
		} else {
			$('.vue_preview_desc').removeClass('preview');
		}
	});

	$('#vue_country').change(function(){
		$('.vue_preview_country').text($(this).val());


		if($(this).val() === "us"){
			$('#vue_index').val(2);
		} else if($(this).val() === "global"){
			$('#vue_index').val(1);
		} else {
			$('#vue_index').val(3);
		}
		$('.vue_preview_country').removeClass('preview');
	}).change();


	$('.next-btn').click(function(){
		var num = parseInt($(this).parent().parent().attr("id").replace('form-', ''));
		if($('#vue_name').val().length == 0 || $('#vue_country').val().length == 0 || $('#vue_short_description').val().length == 0){
			$('#create-submit').trigger('click');
			return;
		}
		colourTitle(num+1);
		if(num == 2){
			$('#create_vue_container').animate({
				width: '55%'},
				500, function() {
				/* stuff to do after animation is complete */
				$('#stocks_chosen_container').fadeIn('fast', function() {
					$('#form-'+num).hide();
					$('#form-'+(num+1)).fadeIn();
					return;
				});
			});
		}else if(num == 3){
			//make the form look nice
			$('#stocks_chosen_container').fadeOut('fast', function() {
				$('#create_vue_container').animate({
					width: '1150px'},
					500, function() {
					/* stuff to do after animation is complete */
						$('#form-'+num).hide();
						$('#form-'+(num+1)).fadeIn();		
						$('#vue-confirm-container').fadeIn('slow');
						return;			
				});
			});
		}
		$('#form-'+num).hide();
		$('#form-'+(num+1)).fadeIn();
		$('#vue-confirm-container').hide();
		$(window).scrollTop(0);
	});

	function colourTitle(num){
		for(var i = 1; i <= num; i++){
			$('#step-'+i+'-text').addClass('title-complete').removeClass('title-current').removeClass('title-incomplete');;
		}
		for(var j = num; j <= 4; j++){
			$('#step-'+j+'-text').addClass('title-incomplete').removeClass('title-current').removeClass('title-complete');
		}	

		$('#step-'+num+"-text").addClass('title-current');
	}

	$('.prev-btn').click(function(){
		var num = parseInt($(this).parent().parent().attr("id").replace('form-', ''));
		colourTitle(num-1);
		if(num == 4){
			$('#create_vue_container').animate({
				width: '55%'},
				500, function() {
				/* stuff to do after animation is complete */
				$('#stocks_chosen_container').fadeIn('fast', function() {
					$('#form-'+num).hide();
					$('#form-'+(num-1)).fadeIn();
					return;
				});
			});
		} else if(num == 3){
			//make the form look nice
			$('#stocks_chosen_container').fadeOut('fast', function() {
				$('#create_vue_container').animate({
					width: '1150px'},
					500, function() {
					/* stuff to do after animation is complete */
						$('#form-'+num).hide();
						$('#form-'+(num-1)).fadeIn();		
						$('#vue-confirm-container').fadeIn('slow');
						return;			
				});
			});
		}
		$('#form-'+num).hide();
		$('#form-'+(num-1)).fadeIn();
	});

	function fix_row_colours() {
		var i = 0;
		$('.stocks_list tr').each(function() {
			var id = this.id.replace('stockchosen','');
			if ($.inArray(id, currStocks) > -1) {
				if (i%2 == 1)
					$(this).addClass('gray-background');	
				else
					$(this).removeClass('gray-background');
				i++;
			}
		});
		i = 1;
		$('.stocks_preview_list tr').each(function() {
			var id = this.id.replace('stockchosen','');
			if ($.inArray(id, currStocks) > -1) {
				if (i%2 == 1)
					$(this).addClass('gray-background');	
				else
					$(this).removeClass('gray-background');
				i++;
			}
		});
	}
	
	function add_stock(stock) {
		currStocks.push(stock);
		$('#stock_list').val(currStocks);
		fix_row_colours();
		return true;
	}
	
	function remove_stock(stock) {
		var position = $.inArray(stock, currStocks);
		currStocks.splice(position, 1);
		$('#stock_list').val(currStocks);
		fix_row_colours();
		return true;
	}
	
	$('.stocks_container_content').on('keyup', function() {
		if ($('#'+this.id).val() == '') {
			$('#'+this.id+'_instocks').text($('#'+this.id).attr('placeholder'));
			$('#'+this.id+'_instocks').css('background', '#FFF');
		} else {
			$('#'+this.id+'_instocks').text($('#'+this.id).val());
			$('#'+this.id+'_instocks').css('background', '#DDFCC7');
		}
	});
	
	$("#stock_options").on('click','.stock_option', function() {
		var id = this.id.replace('stock','');
		if($.inArray(id, currStocks) != -1){
			if(!appendedError){
				$('#stock_options').append('<div class="clear"></div><p class="stock_error" style="margin-left: 20px; color: #ff0000;">This stock is already in your list</p>');
				appendedError = true;
			}
			return; 
		}
		appendedError = false;
		$('.stock_error').remove();
		jQuery('#'+this.id).fadeOut(100);
		var name = $('#stockname'+id).text();
		var string = '<tr id="stockchosen'+id+'"><td>'+name+'</td><td>'
		+'<a href="#" class="remove_stock_chosen" id="'+id+'"><i class="fa fa-times"></i></a></td></tr>';
		var preview_string = '<tr id="stockchosen'+id+'"><td>'+name+'</td></tr>'
		jQuery('.stocks_list').append(string);
		$('.stocks_preview_list').append(preview_string);
		add_stock(this.id.replace('stock',''));
	});
	
	$(".stocks_list").on('click','.remove_stock_chosen', function() {
		jQuery('#stockchosen'+this.id).remove();
		jQuery('#stockchosen'+this.id).remove();
		remove_stock(this.id.replace('stock',''));
		return false;		
	});
	
	$('#stock_search_submit').on('click', function() {
		submit_stock_search();
	});
	
	$('#vue_stock').on('keyup', function(e) {
		console.log('true');
		if (e.keyCode == 13)
			submit_stock_search();
	});
	
	function submit_stock_search() {
		var url = $('#vue_stock').val();
		var index = $("#vue_index option:selected").val();
		if (url == '') {
			$('#stock_options').html('<p style="margin-left: 20px; color: #ff0000;"><i class="fa fa-warning"></i> Please enter a ticker to search.</p>');
			return false;
		}
		$.ajax({
			type:'GET',
			url: '/getstocks/'+url+'/'+index,
			async: false,
			dataType: "json",
			data:{},
			success: function(response) {
				$('#stock_options').hide();
				var output = '';
				for (var property in response) {				
					if ($.inArray(response[property][0], currStocks) != -1) {
						continue;
					}
					output += '<div class="stock_option" id="stock'+response[property][0]+'"><span id="stockname'+response[property][0]+'">'+response[property][1]+'</span>: '+response[property][3]+'</div>';
				}
				if (output == '') {
					output = '<p style="margin-left: 20px">No results, please try again.</p>';
				}
				$('#stock_options').html(output);
				$('#stock_options').fadeIn(400);
			}
		});
		return true;
	}
	
});