$(function() {
  $(document).ajaxStart(function() {
    $("#loader").show();
  });

  $(document).ajaxStop(function() {
    $("#loader").hide();
  });

  $('#email-form').submit(function(e){
    $.post('/subscribe/', $(this).serialize(), function(data) {
      if(data == ""){
        $("#email").val("");
        $("#thanks-note-modal").modal("show");
      }else{
        emailError = $("#email-error");
        emailError.html(data);
        emailError.show();
        emailError.delay(5000).fadeOut();
      }
    })
    e.preventDefault();
  });

  var successFlashMsg = $('span.text-success');
  var errorFlashMsg = $('span.text-error');
  if (!successFlashMsg.is(':empty')) {
    msgContainer = successFlashMsg.parent('.alert');
    msgContainer.show();
    msgContainer.delay(5000).fadeOut();
  }
  if (!errorFlashMsg.is(':empty')) {
    msgContainer = errorFlashMsg.parent('.alert');
    msgContainer.show();
    msgContainer.delay(5000).fadeOut();
  }

});
