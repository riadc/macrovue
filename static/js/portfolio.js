var chartshown;
var displayby;
$(document).ready(function(){
	$('#tab2-content').hide();
	$('.graph').hide();
	$('#chartdollar-all').show();
	chartshown = "all";
	$('.percent').hide();
	$('#all-btn').addClass('selected');
	// change the page tab
	$('.tab').on('click', function() {
		var id = this.id;
		$('.tab-content').hide();
		$('#'+id+'-content').show();
		$('.tab').removeClass('green_haze active-tab');
		$('#'+id).addClass('green_haze active-tab');
	});

	$('#display-select').change(function(){
		if($(this).val() == 2){
			//percentage 
			displayby = 1;
			$('#chartdollar-'+chartshown).hide();
			$('#chartpercent-'+chartshown).show();
			$('.dollar').hide();
			$('.percent').show();
			//fixes the graph size issue
			$(window).resize();
		} else {
			displayby = 0;
			$('#chartpercent-'+chartshown).hide();			
			$('#chartdollar-'+chartshown).show();
			$('.percent').hide();
			$('.dollar').show();
			//fixes the graph size issue.
			$(window).resize();
		}
	});
  

	$('.selection-btn').on('click', function(){
		//if this actually has a graph, switch to it.
		if(!($(this).hasClass('faded'))){
			//buttons
			$('.selection-btn').removeClass('selected');
			$(this).addClass('selected');
			chartshown = this.id.replace('-btn', '');
			//graph
			if(displayby == 1){
				//percent
				$('.graph').hide();
				$('#chartpercent-'+chartshown).show();
				$(window).resize();
			} else {
				//dollar
				$('.graph').hide();
				$('#chartdollar-'+chartshown).show();
				$(window).resize();
			}

		} else {
			//button is blank, there is no graph associated. 
		}

	});
});

