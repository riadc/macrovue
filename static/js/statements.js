$(document).ready(function(){
	$('.tab-content').hide();
	$('#tab1-content').show();
	// change the page tab
	$('.tab').on('click', function() {
		var id = this.id;
		$('.tab-content').hide();
		$('#'+id+'-content').show();
		$('.tab').removeClass('green_haze active-tab');
		$('#'+id).addClass('green_haze active-tab');
	});
});