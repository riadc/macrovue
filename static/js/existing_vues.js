/*
	Ensure #page_num is kept updated.
	Two sections:
		Filtered
			- When we filter, we always start on page 0
			- Although, when we click next page, we should know what is current page
		Non-Filtered
			- When a page is clicked, keep updated to know what is current page
		-- Knowing curr page lets us not load the same page again (maximise resources)
*/

function build_pages(total_vues,curr_page,page_amt) {
	var num_pages = Math.ceil(total_vues/page_amt);
	var pages_output = '';
	for (var i = 0; i < num_pages; i++) {
		var curr_pagenumber = i+1;
		if (i == curr_page) {
			pages_output += '<a href="#" class="page_link strong_text" id="page'+i+'">'+curr_pagenumber+'</a> &nbsp; ';	
		} else {
			pages_output += '<a href="#" class="page_link" id="page'+i+'">'+curr_pagenumber+'</a> &nbsp; ';
		}
	}
	$('#pages_output').html(pages_output);
}

function getNextPage(page_num, page_amt) {

	var filters = [];
	$('.search_existing_vues').each(function() {
		if ($('#'+this.id).prop('checked'))
			filters.push(this.id.replace('checkbox',''));
	});

	// grab all the possible vues
	var vues = [];
	for (var i = 0; i < filters.length; i++) {
		$.ajax({
			type:'GET',
			url: '/getvues/'+filters[i],
			async: false,
			dataType: "html",
			data:{},
			success: function(response) {
				response = jQuery.parseJSON(response.replace(", }","}"));
				for (var property in response) {
					vues.push(response[property]);
				}
			}
		});
	}
	
	// i represents if we need to find uniques (ie: more than one menu)
	if (i > 1) {
		// now, get the uniques ONLY
		var uniques = [], actual_uniques = [];
		for (var i = 0; i < vues.length; i++) {
			if (uniques.indexOf(vues[i][0]) == -1)
				uniques.push(vues[i][0]);
			else
				actual_uniques.push(vues[i]);
		}
		
		var starting_num = parseInt(page_num)*page_amt;
		var is_more_after = (parseInt(page_num)+1)*page_amt;
		
		var html_output = '';
		for (var i = 0; i < actual_uniques.length; i++) {
			if (i >= is_more_after)
				break;
			if (i >= starting_num) {
				single_vue = vues[i];
				if(single_vue[3] > 0){
					html_output += 
						"<a href=\"/vues/"+single_vue[1].replace(' ','-')+"\"><div class=\"existing_vue\" id=\"vue"+single_vue[0]+"\">"+
							"<div class=\"existing_vue_info\" style=\"background: url('"+single_vue[2]+"');\">" +
								"<div class=\"existing_vue_title\">" +
									single_vue[1]+
								"</div>" +
							"</div>" +
							"<div class=\"existing_vue_return\">" +
								"<span class=\"float_left\" style=\"font-size: 16px; padding-top: 18px; padding-left: 10px;\">One month return</span>" +
								"<span class=\"green_return float_right\" style=\"padding-right: 10px\"><i class=\"fa fa-arrow-up\" style=\"font-size: 26px\"></i> "+single_vue[3]+"%</span>" +
							"</div>" +
						"</div></a>";	
				} else if(single_vue[3] < 0){
					html_output += 
						"<a href=\"/vues/"+single_vue[1].replace(' ','-')+"\"><div class=\"existing_vue\" id=\"vue"+single_vue[0]+"\">"+
							"<div class=\"existing_vue_info\" style=\"background: url('"+single_vue[2]+"');\">" +
								"<div class=\"existing_vue_title\">" +
									single_vue[1]+
								"</div>" +
							"</div>" +
							"<div class=\"existing_vue_return\">" +
								"<span class=\"float_left\" style=\"font-size: 16px; padding-top: 18px; padding-left: 10px;\">One month return</span>" +
								"<span class=\"red_return float_right\" style=\"padding-right: 10px\"><i class=\"fa fa-arrow-down\" style=\"font-size: 26px\"></i> "+single_vue[3]+"%</span>" +
							"</div>" +
						"</div></a>";	
				} else {
					html_output += 
						"<a href=\"/vues/"+single_vue[1].replace(' ','-')+"\"><div class=\"existing_vue\" id=\"vue"+single_vue[0]+"\">"+
							"<div class=\"existing_vue_info\" style=\"background: url('"+single_vue[2]+"');\">" +
								"<div class=\"existing_vue_title\">" +
									single_vue[1]+
								"</div>" +
							"</div>" +
							"<div class=\"existing_vue_return\">" +
								"<span class=\"float_left\" style=\"font-size: 16px; padding-top: 18px; padding-left: 10px;\">One month return</span>" +
								"<span class=\"neutral_return float_right\" style=\"padding-right: 10px\"><i class=\"fa fa-arrow-up\" style=\"font-size: 26px\"></i> "+single_vue[3]+"%</span>" +
							"</div>" +
						"</div></a>";	
				}
			}			
		}
		$('#dynamic_vues').html(html_output);
		return true;
	} else {
		var starting_num = parseInt(page_num)*page_amt;
		var is_more_after = (parseInt(page_num)+1)*page_amt;
		
		var html_output = '';
		for (var i = 0; i < vues.length; i++) {
			if (i >= is_more_after)
				break;
			if (i >= starting_num) {
				single_vue = vues[i];
				if(single_vue[3] > 0){
					html_output += 
						"<a href=\"/vues/"+single_vue[1].replace(' ','-')+"\"><div class=\"existing_vue\" id=\"vue"+single_vue[0]+"\">"+
							"<div class=\"existing_vue_info\" style=\"background: url('"+single_vue[2]+"');\">" +
								"<div class=\"existing_vue_title\">" +
									single_vue[1]+
								"</div>" +
							"</div>" +
							"<div class=\"existing_vue_return\">" +
								"<span class=\"float_left\" style=\"font-size: 16px; padding-top: 18px; padding-left: 10px;\">One month return</span>" +
								"<span class=\"green_return float_right\" style=\"padding-right: 10px\"><i class=\"fa fa-arrow-up\" style=\"font-size: 26px\"></i> "+single_vue[3]+"%</span>" +
							"</div>" +
						"</div></a>";
				} else if(single_vue[3] < 0){
					html_output += 
						"<a href=\"/vues/"+single_vue[1].replace(' ','-')+"\"><div class=\"existing_vue\" id=\"vue"+single_vue[0]+"\">"+
							"<div class=\"existing_vue_info\" style=\"background: url('"+single_vue[2]+"');\">" +
								"<div class=\"existing_vue_title\">" +
									single_vue[1]+
								"</div>" +
							"</div>" +
							"<div class=\"existing_vue_return\">" +
								"<span class=\"float_left\" style=\"font-size: 16px; padding-top: 18px; padding-left: 10px;\">One month return</span>" +
								"<span class=\"red_return float_right\" style=\"padding-right: 10px\"><i class=\"fa fa-arrow-down\" style=\"font-size: 26px\"></i> "+single_vue[3]+"%</span>" +
							"</div>" +
						"</div></a>";
				} else {
					html_output += 
						"<a href=\"/vues/"+single_vue[1].replace(' ','-')+"\"><div class=\"existing_vue\" id=\"vue"+single_vue[0]+"\">"+
							"<div class=\"existing_vue_info\" style=\"background: url('"+single_vue[2]+"');\">" +
								"<div class=\"existing_vue_title\">" +
									single_vue[1]+
								"</div>" +
							"</div>" +
							"<div class=\"existing_vue_return\">" +
								"<span class=\"float_left\" style=\"font-size: 16px; padding-top: 18px; padding-left: 10px;\">One month return</span>" +
								"<span class=\"neutral_return float_right\" style=\"padding-right: 10px\"> -- "+single_vue[3]+"%</span>" +
							"</div>" +
						"</div></a>";
				}
			}			
		}
		// do we output to all or just dynamic?
		if (filters.indexOf('0') == -1)
			$('#dynamic_vues').html(html_output);
		else
			$('#all_vues').html(html_output);
		return true;
	}
}

jQuery(document).ready(function($) {
	var page_amt = $('#page_amt').val();
	var vue_count = $('#vue_count').val();

	build_pages(vue_count,0,page_amt);

	$("body").on('click','.page_link', function() {
		var page_num = this.id.replace('page','');
		
		// no point clicking it again
		if (page_num == $('#page_num').val())
			return false;
		
		// remove all bolding
		$('.page_link').removeClass('strong_text');
		
		// add to correct one
		$('#'+this.id).addClass('strong_text');
		
		// set the page num
		$('#page_num').val(page_num);

		// yes it does, so lets generate them
		getNextPage(page_num, page_amt);
		return false;
	});
	
	// search the existing vues
	// generate from beginning (ie: page 0)
	$('.search_existing_vues').on('click', function() {
	
		// clear up dynamic vue
		$('#dynamic_vues').html('');
		$('#dynamic_vues').hide();

		// get the menu url
		var url = this.id.replace('checkbox','');

		var no_filter = true;
		$('.search_existing_vues').each(function() {
			if ($('#'+this.id).prop('checked'))
				no_filter = false;
		});
		if (no_filter)
			url = 0;
		
		// if its 0 that means it's showing all the vues
		if (url == 0) {
			// what page are we on?
			var counter = 0;
			$('#all_vues > a').each(function() {
				counter++;
			});
			counter -= 6;

			build_pages(vue_count, counter/page_amt, page_amt);
			$('#page_num').val(counter/page_amt);

			// fix checkboxes and hide dynamic vues
			$('.search_existing_vues').prop('checked', false);
			$('#checkbox0').prop('checked', true);		
			$('#dynamic_vues').hide();
			$('#all_vues').fadeIn(400);
		} else {
			// update the page num
			$('#page_num').val(0);
			// hide the vues
			$('#all_vues').hide();
			// request the content of the data
			$('#checkbox0').prop('checked', false);
			var total_output = [];
			// how many menus are there?
			var i = 0;
			$('.search_existing_vues').each(function() {
				if (!$('#'+this.id).prop('checked'))
					return true;
				url = this.id.replace('checkbox','');
				$.ajax({
					type:'GET',
					url: '/getvues/'+url,
					async: false,
					dataType: "html",
					data:{},
					success: function(response) {
						response = jQuery.parseJSON(response.replace(", }","}"));
						for (var property in response) {
							total_output.push(response[property]);
						}
					}
				});
				i++;
			});
			var html_output = '';
			var single_vue = '';
			// how many on one page
			var page_num = 1;
			
			build_pages(total_output.length, 0, page_amt);

			if (i > 1) {
				var current_elements = [];
				for (var i = 0; i < total_output.length; i++) {
					if (i < page_amt*page_num) {
						if (current_elements.indexOf(total_output[i][1]) == -1) {
							current_elements.push(total_output[i][1]);
						} else {
							single_vue = total_output[i];
							if(single_vue[3] > 0){
								html_output += 
									"<a href=\"/vues/"+single_vue[1].replace(' ','-')+"\"><div class=\"existing_vue\" id=\"vue"+single_vue[0]+"\">"+
										"<div class=\"existing_vue_info\" style=\"background: url('"+single_vue[2]+"');\">" +
											"<div class=\"existing_vue_title\">" +
												single_vue[1]+
											"</div>" +
										"</div>" +
										"<div class=\"existing_vue_return\">" +
											"<span class=\"float_left\" style=\"font-size: 16px; padding-top: 18px; padding-left: 10px;\">One month return</span>" +
											"<span class=\"green_return float_right\" style=\"padding-right: 10px\"><i class=\"fa fa-arrow-up\" style=\"font-size: 40px\"></i> "+single_vue[3]+"%</span>" +
										"</div>" +
									"</div></a>";
							} else if(single_vue[3] < 0){
								html_output += 
									"<a href=\"/vues/"+single_vue[1].replace(' ','-')+"\"><div class=\"existing_vue\" id=\"vue"+single_vue[0]+"\">"+
										"<div class=\"existing_vue_info\" style=\"background: url('"+single_vue[2]+"');\">" +
											"<div class=\"existing_vue_title\">" +
												single_vue[1]+
											"</div>" +
										"</div>" +
										"<div class=\"existing_vue_return\">" +
											"<span class=\"float_left\" style=\"font-size: 16px; padding-top: 18px; padding-left: 10px;\">One month return</span>" +
											"<span class=\"red_return float_right\" style=\"padding-right: 10px\"><i class=\"fa fa-arrow-down\" style=\"font-size: 40px\"></i> "+single_vue[3]+"%</span>" +
										"</div>" +
									"</div></a>";
							} else {
								html_output += 
									"<a href=\"/vues/"+single_vue[1].replace(' ','-')+"\"><div class=\"existing_vue\" id=\"vue"+single_vue[0]+"\">"+
										"<div class=\"existing_vue_info\" style=\"background: url('"+single_vue[2]+"');\">" +
											"<div class=\"existing_vue_title\">" +
												single_vue[1]+
											"</div>" +
										"</div>" +
										"<div class=\"existing_vue_return\">" +
											"<span class=\"float_left\" style=\"font-size: 16px; padding-top: 18px; padding-left: 10px;\">One month return</span>" +
											"<span class=\"neutral_return float_right\" style=\"padding-right: 10px\"> -- "+single_vue[3]+"%</span>" +
										"</div>" +
									"</div></a>";
							}
						}
					} else {
						break;
					}
				}
			} else {
				for (var i = 0; i < total_output.length; i++) {
					if (i < page_amt*page_num) {
						single_vue = total_output[i];
						if(single_vue[3] > 0){
							html_output += 
								"<a href=\"/vues/"+single_vue[1].replace(' ','-')+"\"><div class=\"existing_vue\" id=\"vue"+single_vue[0]+"\">"+
									"<div class=\"existing_vue_info\" style=\"background: url('"+single_vue[2]+"');\">" +
										"<div class=\"existing_vue_title\">" +
											single_vue[1]+
										"</div>" +
									"</div>" +
									"<div class=\"existing_vue_return\">" +
										"<span class=\"float_left\" style=\"font-size: 16px; padding-top: 18px; padding-left: 10px;\">One month return</span>" +
										"<span class=\"green_return float_right\" style=\"padding-right: 10px\"><i class=\"fa fa-arrow-up\" style=\"font-size: 26px\"></i> "+single_vue[3]+"%</span>" +
									"</div>" +
								"</div></a>";
						} else if(single_vue[3] < 0){
							html_output += 
								"<a href=\"/vues/"+single_vue[1].replace(' ','-')+"\"><div class=\"existing_vue\" id=\"vue"+single_vue[0]+"\">"+
									"<div class=\"existing_vue_info\" style=\"background: url('"+single_vue[2]+"');\">" +
										"<div class=\"existing_vue_title\">" +
											single_vue[1]+
										"</div>" +
									"</div>" +
									"<div class=\"existing_vue_return\">" +
										"<span class=\"float_left\" style=\"font-size: 16px; padding-top: 18px; padding-left: 10px;\">One month return</span>" +
										"<span class=\"red_return float_right\" style=\"padding-right: 10px\"><i class=\"fa fa-arrow-down\" style=\"font-size: 26px\"></i> "+single_vue[3]+"%</span>" +
									"</div>" +
								"</div></a>";
						} else {
							html_output += 
								"<a href=\"/vues/"+single_vue[1].replace(' ','-')+"\"><div class=\"existing_vue\" id=\"vue"+single_vue[0]+"\">"+
									"<div class=\"existing_vue_info\" style=\"background: url('"+single_vue[2]+"');\">" +
										"<div class=\"existing_vue_title\">" +
											single_vue[1]+
										"</div>" +
									"</div>" +
									"<div class=\"existing_vue_return\">" +
										"<span class=\"float_left\" style=\"font-size: 16px; padding-top: 18px; padding-left: 10px;\">One month return</span>" +
										"<span class=\"neutral_return float_right\" style=\"padding-right: 10px\"> -- "+single_vue[3]+"%</span>" +
									"</div>" +
								"</div></a>";
						}
					} else {
						break;
					}
				}
			}
			$('#dynamic_vues').append(html_output);
			$('#dynamic_vues').fadeIn(1000);
		}
	});
});