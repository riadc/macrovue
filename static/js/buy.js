var populated = 1;
var orders;
var vue_id;
var minValue;
var numRows;
$(document).ready(function(){

	/* getting the initial largest value */
	numRows = $('#stocks-table tr').size() - 1;
	var highestPrice = 0;
	$('.stock-price').each(function(){
		if(parseFloat($(this).text()) > highestPrice){
			highestPrice = parseFloat($(this).text());
		}
	});

	//after finding the highest price, make sure that this is the minimum value
	/* TODO SET MIN VALUE */ 
	minValue = highestPrice * numRows;
	$('#min-amount').text(minValue.toFixed(2));


	$('#reset-table').click(function(){
		populateTable(minValue);
		$('#amount-invest').val(minValue.toFixed(2));
		$('.inv-amount-dollar').show();
		$('#inputs').css('border', 'none');
		$('#stocks-table input').css('border', '1px solid #ccc');
		$('.input-error').hide();
		calcTotals();
	});

	$('#allocate-btn').click(function(){
		var allocateVal = $('#amount-invest').val();
		if(!($.isNumeric(allocateVal))){
			$("#inputs").css('border', '1px solid #ff0000');
			$('#error-text').text("Please Enter Numbers Only!");
			$('.input-error').show();
		} else {
			if(allocateVal < minValue){
				$("#inputs").css('border', '1px solid #ff0000');
				$('#error-text').text("Please enter a number greater than $" + minValue.toFixed(2));
				$('.input-error').show();
				populateTable(0);
				return;
			} else if(parseInt($("#balance-info").text()) == 0){
				$("#inputs").css('border', '1px solid #ff0000');
				$('#error-text').text("Insufficient funds, please add funds.");
				$('.input-error').show();
				populateTable(0);
				return;				
			} else if(allocateVal > parseInt($('#balance-info').text())){
				$("#inputs").css('border', '1px solid #ff0000');
				$('#error-text').text("Insufficient funds, please enter a number less than $" + $('#balance-info').text() + " or add funds.");
				$('.input-error').show();
				populateTable(0);
				return;
			}
			$('#inputs').css('border', 'none');
			$('#stocks-table input').css('border', '1px solid #ccc');
			$('.input-error').hide();
			populated = 0;
			populateTable(allocateVal);
		}
	});

	$('#stocks-table input').focusout(function(){
		// console.log($(this).parent().parent().parent());
		if(populated == 1){
			var amount;
			if(!($.isNumeric($(this).val()))){
				$(this).css('border', '1px solid #ff0000');
				amount = 0;
			} else {
				$('#stocks-table input').css('border', '1px solid #ccc');
				amount = $(this).val();
				var price = $(this).parent().parent().parent().find('.stock-price').text();
				$(this).parent().parent().parent().find('.quantity').text(Math.floor(parseFloat(amount / price)));
				$(this).parent().parent().parent().find('.invested-amount').text((parseFloat(($(this).parent().parent().parent().find('.quantity').text())) * (price)).toFixed(2));
				$(this).parent().parent().parent().find('.inv-amount-dollar').show();
				getNewAmount();	
			}
		}
	});

	$("#confirm-btn").click(function(){
		// call place_order() function in vues/views.py
		submitOrders();
		$.ajax({
			type: "POST",
			url: "/place_order/",
			datatype: "json",
			data: orders,
			success: function(output){
				window.location = "/orders/";
			},
			error: function(textStatus, errorThrown){
				alert("Status: "+textStatus + "\nError: " + errorThrown);
			}

		});
	});
});

var submitOrders = function(){
    var order_list = [];
    vue_id = $('#vue-id').text();
    var counter = 1;
	$('#stocks-table tr').each(function(){
	    var order = {};
  	    order.stock_ticket = $(this).find('.stock-ticket').text();
  	    order.stock_quantity = $(this).find('.quantity').text();
  	    order.stock_price = $(this).find('.stock-price').text();
  	    if(order.stock_ticket){
  	    	order.stock_id = $('#stock-id'+counter).text();
  	    	counter++;
  		}
  	    if (order.stock_quantity > 0){
  	    	order_list.push(order);
		}
	});
	var param = {}
	param.vue_id = vue_id
	param.order_list = order_list
	orders = JSON.stringify(param);
}

var populateTable = function(val){
	populated = 0;
	$('.inv-amount-dollar').show();
	var numStocks = $('#stocks-table tr').size()-1;
	var allocateNum = val / numStocks;
	$('#stocks-table tr').each(function(){
		$(this).find('.table-amount').val(allocateNum.toFixed(2));
		var price = parseFloat($(this).find('.stock-price').text());
		$(this).find('.quantity').text(Math.floor(allocateNum/price));

		var invamt = parseFloat($(this).find('.quantity').text()) * price;
		$(this).find('.invested-amount').text(invamt.toFixed(2));
	});
	populated = 1;
	calcTotals();
}

var getNewAmount = function(){
	if(populated == 1){
		var newAmount = 0.00;
		var balance = parseFloat($("#balance-info").text());
		$('#stocks-table tr').each(function(){
			var num = parseFloat($(this).find('.table-amount').val());
			if($.isNumeric(num) && num > 0){
				newAmount += num;			
			}
		});

		$('#amount-invest').val(newAmount);
		if(newAmount > balance){
			$("#inputs").css('border', '1px solid #ff0000');
			$('#stocks-table input').css('border', '1px solid #ff0000');
			$('#error-text').text("Insufficient funds, please add funds.");
			$('.input-error').show();
			alert("Insufficient funds, please add funds or enter lower numbers.");
		} else {
			$("#inputs").css('border', 'none');
			$('#stocks-table input').css('border', '1px solid #ccc');
			$('.input-error').hide()
		}
		calcTotals();
	}
}


var calcTotals = function(){
	var totalInv = 0;
	var fees = parseFloat($('#fees-cell').text());
	var count = 0;
	var curr;
	$('#stocks-table tr').each(function(){
		if(count == 0){
			count++;
			return;
		}
		if($(this).find('.invested-amount').text() == ""){
			//dont count NaN. 
		} else {
			totalInv += parseFloat($(this).find('.invested-amount').text());
		}
	});
	$('#total-invested-cell').text("$" + totalInv.toFixed(2));
	$('.fees-container').show();
	$('#total-cell').text("$" + (fees + totalInv).toFixed(2));
}
