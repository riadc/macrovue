/*
	The JS for the profiles
*/

jQuery(document).ready(function($) {

	// change the page tab
	$('.profile_menu_option').on('click', function() {
		var id = this.id.replace('tab','');
		$('.profile_content').hide();
		$('#profile_content_'+id).show();
		$('.profile_menu_option').removeClass('active');
		$('#tab'+id).addClass('active');
	});
});