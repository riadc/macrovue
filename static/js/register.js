jQuery(document).ready(function($) {
	$('#register_button').on('click', function() {
		$('#errors').html('');
		var fname = $('#first_name').val();
		var lname = $('#last_name').val();
		var email = $('#email').val();
		var username = $('#username').val();
		var password = $('#password1').val();
		var password2 = $('#password2').val();
		
		var name_output = verify_name(fname,lname);
		if (name_output != 1) {
			if (name_output == 0)
				output_error('Name fields incorrect');
			else if (name_output == -1)
				output_error('First name field incorrect.');
			else if (name_output == -2)
				output_error('Last name field incorrect.');
		}
		
		var email_output = verify_email(email);
		if (email.length <= 0) {
			output_error("Please enter an email.");
		} else if (!email_output) {
			output_error('Incorrect email format.');
		}
		// if email was accepted by regex
		if (email_output) {
			$.ajax({
				type:'GET',
				url: '/emailcheck/'+email,
				async: false,
				dataType: "text",
				data:{},
				success: function(response) {
					if (response > 0) {
						username_output = 0;
						output_error("This email is already being used.");
					}	
				}
			});		
		}
		
		// verify pwd and provide correct error reporting
		var pwd_output = verify_password(password, password2);
		if (pwd_output != 1) {
			if (pwd_output == 0)
				output_error("Password is too weak.");
			else if (pwd_output == -1)
				output_error("Passwords do not match.");
		}
		
		var username_output = 1;
		if (username.length > 10) {
			username_output = 0;
			output_error("Username cannot be greater than 10 characters.");
		} else if (username.length <= 0) {
			username_output = 0;
			output_error("Please enter a username.");
		} else {
			$.ajax({
				type:'GET',
				url: '/usernamecheck/'+username,
				async: false,
				dataType: "text",
				data:{},
				success: function(response) {
					if (response > 0) {
						username_output = 0;
						output_error("This username has already been taken.");
					}	
				}
			});
		}
		
		if (name_output != 1 || !email_output || pwd_output != 1 || username_output != 1)
		{
			console.log("We failed");
			return false;
		} else
			return true;
	});

	// output an error to the page
	function output_error(error_msg) {
		var error = '<div id="form-error"><p><i class="fa fa-warning"></i> Error: <span id="invalid-item">'+error_msg+'</span></p></div>';
		$("#errors").append(error);
	}
	
	// very if the users name
	function verify_name(fname, lname) {
		if (fname.length < 3 && lname.length < 2)
			return 0;
		if (fname.length < 3)
			return -1;
		if (lname.length < 2)
			return -2;
		return 1;
	}
	
	// verify the email provided
	function verify_email(email) {
		var regexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return regexp.test(email);
	}
	
	// verify the password provided
	function verify_password(password, password_two) {
		var regexp = /^(?=(?:.*[a-z]){2})(?=(?:.*[A-Z]){1})(?=(?:.*\d){1})(?=(?:.*[!()@#$%^&*-]){1}).{8,}$/;
		if (!regexp.test(password))
			return 0;
		if (password != password_two)
			return -1;
		return 1;
	}
});