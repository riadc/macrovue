/*
	The JS for the settings
*/

jQuery(document).ready(function($) {

	// change the page tab
	$('.single_vue_tab').on('click', function() {
		var id = this.id;
		$('.profile_settings').hide();
		$('#content_'+id).show();
		$('.single_vue_tab').removeClass('green_haze single_vue_active_tab');
		$('#'+id).addClass('green_haze single_vue_active_tab');
	});
	$('#id_biography').on('keyup', function() {
		var val = $('#'+this.id).val().length;
		$('#bio_counter').text(val);
	});
});