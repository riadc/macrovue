jQuery(document).ready(function($) {

	var hash = window.location.hash;
	if (hash == '#stocks') {
		$('.tab_content').hide();
		$('#tab3_content').show();
		$('.single_vue_tab').removeClass('green_haze single_vue_active_tab');
		$('#tab3').addClass('green_haze single_vue_active_tab');	
	}
		
	
	var currStocks = [];
	$('.stocks_list tr').each(function() {
		var stock = this.id.replace('row','');
		add_stock(stock);
	});
	
	function add_stock(stock) {
		currStocks.push(stock);
		$('#stock_list').val(currStocks);
		return true;
	}
	
	function remove_stock(stock) {
		var position = $.inArray(stock, currStocks);
		currStocks.splice(position, 1);
		$('#stock_list').val(currStocks);
		return true;
	}

	// The tooltips used on the site	
	//$('.hastip').tooltipsy();

	/*$('.stock_bubble').tooltipsy({
		className: 'arrow_box',
		offset: [0, 10],
		content: function ($el, $tip) {
	        return '<h1>'+$el.context.id+'</h1><hr><p>Testing this content out by posting a lot of words.</p><a href="#">Yahoo Finance</a>';
    	},
    	hide: function (e, $el) {
			$($el).on('mouseleave', function() {
				$($el).fadeOut(100);
			});
		}
	});*/
	
	// change the page tab
	$('.single_vue_tab').on('click', function() {
		var id = this.id;
		$('.tab_content').hide();
		$('#'+id+'_content').show();
		$('.single_vue_tab').removeClass('green_haze single_vue_active_tab');
		$('#'+id).addClass('green_haze single_vue_active_tab');
	});

	// count of sliders
	var slidersCount = $('.nstSlider').length;
	
	// when a nstslider is clicked, we want to ensure that it equals 100%
	$('.nstSlider').on('click', function() {
		var id = this.id;
		var total = 0;
		var value;
		var lastModified;
		// check each leftLabel and add the total weight
		$( ".leftLabel" ).each(function( index ) {
			if (id == this.id.replace('weight',''))
				lastModified = parseInt($(this).text().replace('%',''));
			value = parseInt($(this).text());
  			total += value;
		});
		
		var remainder = total-100;
		var divider = total - lastModified;
		var percentage = remainder/divider;
		
		// update the new weights
		$( ".leftLabel" ).each(function(index) {
			if (id == this.id.replace('weight',''))
				return;
			value = Math.round(parseInt($(this).text())*100*(1-percentage))/100;
			$('#'+this.id).text(value+"%");
			$('#'+this.id.replace('weight','')).nstSlider('set_position',value);
		});
	});
	
	// generate nstslider on page load
	$('.nstSlider').nstSlider({
    	"left_grip_selector": ".leftGrip",
   		"value_changed_callback": function(cause, leftValue, rightValue) {
        	$(this).parent().find('.leftLabel').text(leftValue+".00%");
    	}
	});
	
	$('.vote-down').on('click', function() {
		alert(this.id);
		
		return false;
	});
	
	$('.vote-up').on('click', function() {
		alert(this.id);

		return false;
	});
	
	// generate stocks based on weight strategy
	/*$("#weight_strategy").change(function() {
	    $( "select option:selected" ).each(function() {
			if ($(this).text() == 'Equal') {	
				var value = 0;
				var slidersCount = $('.nstSlider').length;
				$( ".leftLabel" ).each(function(index) {
					value = Math.round((100/slidersCount)*100)/100;
					$('#'+this.id).text(value+"%");
					$('#'+this.id.replace('weight','')).nstSlider('set_position',value);
				});
			}
		});
	});*/

	
	// once we delete a stock, remove it.
	// if there are no more in that group, delete the group
	// reset stock values (needs improvement)
	$("body").on('click','.delete_stock', function() {
		var id = this.id.replace('delete','');
		console.log(id);
		remove_stock(id);
		fix_row_colours();
		$("#row"+id).fadeOut(200);
		return false;	
	});
	
		/*var value = 0;
		var slidersCount = $('.nstSlider').length;
		$( ".leftLabel" ).each(function(index) {
			value = Math.round((100/slidersCount)*100)/100;
			$('#'+this.id).text(value+"%");
			$('#'+this.id.replace('weight','')).nstSlider('set_position',value);
		});*/
		// grab the position of the index and remove it

	// add stocks to the page
	$("body").on('click','.add_sfi', function() {
		var id = this.id;
		id = id.replace('stock','');
		$.ajax({
			type:'GET',
			url: '/getstockfinancials/'+id,
			async: false,
			dataType: "json",
			data:{},
			success: function(response) {
				if ($.inArray(response['data'][1], currStocks) > -1)
					return false;
				// output to the page
				$('#modal_stock_'+id).fadeOut(400); // remove the row
				var output = '<tr id="row'+response['data'][0]+'"><td style="width: 10%; text-align: left !important">'+response['data'][1]+'</td><td style="width: 18%; text-align: left" class="stock_bubble" id="'+response['data'][1]+'" title="'+response['data'][2]+'"> <a href="https://au.finance.yahoo.com/q?p=finance.yahoo.com&s='+response['data'][1]+'">'+response['data'][2]+'</a></td><td style="width: 9%">'+response['data'][3]+'</td><td style="width: 8%">'+response['data'][4]+'%</td><td style="width: 6%">'+response['data'][5]+'</td><td style="width: 8%">'+response['data'][6]+'</td><td style="width: 12%">'+response['data'][7]+'%</td><td style="width: 6%">'+response['data'][8]+'%</td><td style="width: 6%">'+response['data'][9]+'%</td><td><a href="#" class="delete_stock" id="delete'+response['data'][0]+'"><i class="fa fa-times"></i></a></td></tr>';
				$('.stocks_list').append(output);
				// add to current stocks
				add_stock(response['data'][0]);
			}
		});
		fix_row_colours();
		return false;
	});
	

	// Rating 
	$('.rate .circle').on('mouseenter', function() {
		var circleNum = this.id.replace('circle','');
		for (var i = 0; i < circleNum; i++) {
			$('.rate #circle'+i).addClass('circle_hover');
		}
	}).on('mouseout', function() {
		var circleNum = this.id.replace('circle','');
		for (var i = 0; i < circleNum; i++) {
			$('#circle'+i).removeClass('circle_hover');
		}
	});
	
	//not processing circles on success due to hang time. 
	$('.rate .circle').on('click', function() {
		var vue_id = $('#vue_id_hidden').val();
		var rating = this.id.replace('circle','');
		for(var i = rating; i <= 5; i++){
			$('#circle'+i).removeClass('circle_full');
			$('#circle'+i).addClass('circle_empty');
		}
		for (var i = 0; i <= rating; i++) {
			$('.rate #circle'+i).addClass('circle_full');
		}
		$('#your_rating').text(rating+'/5');

		submit_rating(vue_id, rating);
	});
	
	function submit_rating(vue_id, rating){
		$.ajax({
			type:'GET',
			url: '/vues/submit_rating/'+ vue_id +'/'+ rating,
			async: false,
			dataType: "text"
		});
	}

	// search when a user clicks enter
	$('#enter_ticker').on('keydown', function(e) {
		if (e.keyCode == 13)
			submit_search();
	});
	
	// search when a user clicks the search button
	$("#search_stocks").on('click', function() {
		submit_search();
	});
	
	// search the stocks using ajax 
	function submit_search() {
		var index = $('#vue_index_hidden').val();
		var url = $('#enter_ticker').val();
		console.log(index);
		$.ajax({
			type:'GET',
			url: '/getstocks/'+url+'/'+index,
			async: false,
			dataType: "json",
			data:{},
			success: function(response) {
				$('#stock_add_display_modal').hide();
				var output = '';
				for (var property in response) {				
					if (!$.inArray(response[property][1], currStocks)) {
						output += "<div class=\"stock_modal_exists\"><h3>"+response[property][1] + " is already included in this vue.</h3></div>";
						continue;
					}
					output += '<div class="stock_modal_row" id="modal_stock_'+response[property][0]+'"><div class="stock_modal_row_content">'+
					response[property][1]+": "+response[property][2] +
					'</div><div class="stock_modal_row_plus white_to_lightgray add_sfi" id="'+response[property][0]+'stock">+</div></div><div class="clear"></div>';
				}
				$('#stock_add_display_modal').html(output);
				$('#stock_add_display_modal').fadeIn(400);
			}
		});	
	}
	
	/*
		Loop through each tr, if its a current stock, update the background
	*/
	// reevaluate the row colourings
	function fix_row_colours() {
		var i = 0;
		$('.stocks_list tr').each(function() {
			var id = this.id.replace('row','');
			if ($.inArray(id, currStocks) > -1) {
				if (i%2 == 1)
					$(this).addClass('gray-background');	
				else
					$(this).removeClass('gray-background');
				i++;
			}
		});
	}
	
	// click button to submit
	$('#id_submit').on('click', function() {
		submit_comment();
	});
	
	// enter button to submit
	$('#id_comment').on('keydown', function(e) {
		if (e.keyCode == 13)
			submit_comment();
	});
	
	// submit the comment with live errors
	function submit_comment() {
		var comment = $('#id_comment').val();
		if (comment != '') {
			$("#vue_comment").submit();
			$('#id_comment').val('');
		} else {
			$('#id_comment').css({'background':'rgba(255,0,0,0.1)','border':'1px solid rgba(255,0,0,0.1)'});
			$('#id_comment').addClass('error_comment');
			$('#id_comment').attr("placeholder", "This field cannot be empty");
		}
	}
});