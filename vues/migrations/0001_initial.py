# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Menu'
        db.create_table('menu', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('type', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vues.Menu'], null=True, blank=True)),
        ))
        db.send_create_signal(u'vues', ['Menu'])

        # Adding model 'Stocks'
        db.create_table('stocks', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('factset_id', self.gf('django.db.models.fields.CharField')(default='', max_length=20)),
            ('macrovue_id', self.gf('django.db.models.fields.CharField')(default='', max_length=40)),
            ('index', self.gf('django.db.models.fields.CharField')(default='', max_length=20)),
            ('industry', self.gf('django.db.models.fields.CharField')(default='', max_length=40)),
            ('sector', self.gf('django.db.models.fields.CharField')(default='', max_length=40)),
            ('cusip', self.gf('django.db.models.fields.CharField')(default='', max_length=20)),
            ('sedol', self.gf('django.db.models.fields.CharField')(default='', max_length=20)),
            ('isin', self.gf('django.db.models.fields.CharField')(default='', max_length=20)),
            ('exchange', self.gf('django.db.models.fields.CharField')(default='', max_length=40)),
            ('country', self.gf('django.db.models.fields.CharField')(default='', max_length=40)),
            ('currency', self.gf('django.db.models.fields.CharField')(default='', max_length=20)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=120)),
            ('ticker', self.gf('django.db.models.fields.CharField')(default='', max_length=10)),
        ))
        db.send_create_signal(u'vues', ['Stocks'])

        # Adding model 'StockFinancialData'
        db.create_table('stock_financial_data', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('stock', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vues.Stocks'], null=True)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('dividend_yield', self.gf('django.db.models.fields.DecimalField')(max_digits=35, decimal_places=7)),
            ('price_equity_ratio', self.gf('django.db.models.fields.BigIntegerField')(default='0')),
            ('mkt_cap', self.gf('django.db.models.fields.BigIntegerField')(default=0)),
            ('one_year_vol', self.gf('django.db.models.fields.DecimalField')(default='0', max_digits=10, decimal_places=7)),
            ('one_mo_return', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=7)),
            ('one_yr_return', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=7)),
            ('closing_price', self.gf('django.db.models.fields.DecimalField')(default='0', max_digits=15, decimal_places=7)),
            ('valuation', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('growth', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('volatility', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'vues', ['StockFinancialData'])

        # Adding model 'Vues'
        db.create_table('vues', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('user_generated', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('description', self.gf('django.db.models.fields.CharField')(default='', max_length=1000)),
            ('short_description', self.gf('django.db.models.fields.CharField')(default='', max_length=200)),
            ('analysis', self.gf('django.db.models.fields.TextField')(null=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True)),
            ('country', self.gf('django.db.models.fields.CharField')(default='U.S', max_length=30)),
            ('created_at', self.gf('django.db.models.fields.DateField')(auto_now_add=True, null=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateField')(auto_now=True, null=True, blank=True)),
            ('background', self.gf('django.db.models.fields.CharField')(default='http://www.beck-technology.com/images/bt_image00_home.jpg', max_length=150)),
            ('is_visible', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'vues', ['Vues'])

        # Adding model 'VueVotes'
        db.create_table('vue_votes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('vue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vues.Vues'])),
            ('type', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'vues', ['VueVotes'])

        # Adding model 'VueStocks'
        db.create_table('vue_stocks', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vues.Vues'], null=True)),
            ('stock', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vues.Stocks'], null=True)),
        ))
        db.send_create_signal(u'vues', ['VueStocks'])

        # Adding model 'IndexReturns'
        db.create_table('IndexReturns', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('stock', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vues.Stocks'], null=True)),
        ))
        db.send_create_signal(u'vues', ['IndexReturns'])

        # Adding model 'VueReturns'
        db.create_table('vue_returns', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vues.Vues'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('daily_return', self.gf('django.db.models.fields.DecimalField')(max_digits=22, decimal_places=20)),
            ('cum_return', self.gf('django.db.models.fields.DecimalField')(max_digits=22, decimal_places=20)),
            ('index_return', self.gf('django.db.models.fields.DecimalField')(max_digits=22, decimal_places=20)),
        ))
        db.send_create_signal(u'vues', ['VueReturns'])

        # Adding model 'VueMenus'
        db.create_table('vue_menus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('menu', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vues.Menu'])),
            ('vue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vues.Vues'])),
        ))
        db.send_create_signal(u'vues', ['VueMenus'])

        # Adding model 'VueFinancialData'
        db.create_table('vue_financial_data', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vues.Vues'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('dividend_yield', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=2)),
            ('valuation', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('growth', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('volatility', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('one_mo_return', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=2)),
        ))
        db.send_create_signal(u'vues', ['VueFinancialData'])

        # Adding model 'UserActivated'
        db.create_table('user_activated', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('isActivated', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('randomText', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal(u'vues', ['UserActivated'])

        # Adding model 'UserFavourites'
        db.create_table('user_favourites', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('vue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vues.Vues'])),
        ))
        db.send_create_signal(u'vues', ['UserFavourites'])


    def backwards(self, orm):
        # Deleting model 'Menu'
        db.delete_table('menu')

        # Deleting model 'Stocks'
        db.delete_table('stocks')

        # Deleting model 'StockFinancialData'
        db.delete_table('stock_financial_data')

        # Deleting model 'Vues'
        db.delete_table('vues')

        # Deleting model 'VueVotes'
        db.delete_table('vue_votes')

        # Deleting model 'VueStocks'
        db.delete_table('vue_stocks')

        # Deleting model 'IndexReturns'
        db.delete_table('IndexReturns')

        # Deleting model 'VueReturns'
        db.delete_table('vue_returns')

        # Deleting model 'VueMenus'
        db.delete_table('vue_menus')

        # Deleting model 'VueFinancialData'
        db.delete_table('vue_financial_data')

        # Deleting model 'UserActivated'
        db.delete_table('user_activated')

        # Deleting model 'UserFavourites'
        db.delete_table('user_favourites')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'vues.indexreturns': {
            'Meta': {'object_name': 'IndexReturns', 'db_table': "'IndexReturns'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vues.Stocks']", 'null': 'True'})
        },
        u'vues.menu': {
            'Meta': {'object_name': 'Menu', 'db_table': "'menu'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vues.Menu']", 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'vues.stockfinancialdata': {
            'Meta': {'object_name': 'StockFinancialData', 'db_table': "'stock_financial_data'"},
            'closing_price': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'max_digits': '15', 'decimal_places': '7'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'dividend_yield': ('django.db.models.fields.DecimalField', [], {'max_digits': '35', 'decimal_places': '7'}),
            'growth': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mkt_cap': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'one_mo_return': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '7'}),
            'one_year_vol': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'max_digits': '10', 'decimal_places': '7'}),
            'one_yr_return': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '7'}),
            'price_equity_ratio': ('django.db.models.fields.BigIntegerField', [], {'default': "'0'"}),
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vues.Stocks']", 'null': 'True'}),
            'valuation': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'volatility': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'vues.stocks': {
            'Meta': {'object_name': 'Stocks', 'db_table': "'stocks'"},
            'country': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40'}),
            'currency': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'}),
            'cusip': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'}),
            'exchange': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40'}),
            'factset_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'}),
            'industry': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40'}),
            'isin': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'}),
            'macrovue_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '120'}),
            'sector': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40'}),
            'sedol': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'}),
            'ticker': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'})
        },
        u'vues.useractivated': {
            'Meta': {'object_name': 'UserActivated', 'db_table': "'user_activated'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'isActivated': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'randomText': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'vues.userfavourites': {
            'Meta': {'object_name': 'UserFavourites', 'db_table': "'user_favourites'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'vue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vues.Vues']"})
        },
        u'vues.vuefinancialdata': {
            'Meta': {'object_name': 'VueFinancialData', 'db_table': "'vue_financial_data'"},
            'date': ('django.db.models.fields.DateField', [], {}),
            'dividend_yield': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'growth': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'one_mo_return': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'}),
            'valuation': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'volatility': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'vue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vues.Vues']"})
        },
        u'vues.vuemenus': {
            'Meta': {'object_name': 'VueMenus', 'db_table': "'vue_menus'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vues.Menu']"}),
            'vue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vues.Vues']"})
        },
        u'vues.vuereturns': {
            'Meta': {'object_name': 'VueReturns', 'db_table': "'vue_returns'"},
            'cum_return': ('django.db.models.fields.DecimalField', [], {'max_digits': '22', 'decimal_places': '20'}),
            'daily_return': ('django.db.models.fields.DecimalField', [], {'max_digits': '22', 'decimal_places': '20'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index_return': ('django.db.models.fields.DecimalField', [], {'max_digits': '22', 'decimal_places': '20'}),
            'vue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vues.Vues']"})
        },
        u'vues.vues': {
            'Meta': {'object_name': 'Vues', 'db_table': "'vues'"},
            'analysis': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'background': ('django.db.models.fields.CharField', [], {'default': "'http://www.beck-technology.com/images/bt_image00_home.jpg'", 'max_length': '150'}),
            'country': ('django.db.models.fields.CharField', [], {'default': "'U.S'", 'max_length': '30'}),
            'created_at': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '1000'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'menu_option': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['vues.Menu']", 'through': u"orm['vues.VueMenus']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'short_description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200'}),
            'stocks': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['vues.Stocks']", 'through': u"orm['vues.VueStocks']", 'symmetrical': 'False'}),
            'updated_at': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True'}),
            'user_generated': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'votes': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'votes'", 'symmetrical': 'False', 'through': u"orm['vues.VueVotes']", 'to': u"orm['auth.User']"})
        },
        u'vues.vuestocks': {
            'Meta': {'object_name': 'VueStocks', 'db_table': "'vue_stocks'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vues.Stocks']", 'null': 'True'}),
            'vue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vues.Vues']", 'null': 'True'})
        },
        u'vues.vuevotes': {
            'Meta': {'object_name': 'VueVotes', 'db_table': "'vue_votes'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'vue': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vues.Vues']"})
        }
    }

    complete_apps = ['vues']