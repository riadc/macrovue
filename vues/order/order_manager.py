from ib.ext.Contract import Contract
from ib.ext.Order import Order
from ib.opt import Connection, message

from datetime import datetime
from vues.models import UserStocksOrders, StocksOrdersStatus, CommissionFee, UserStocksHistory

class OrderManager:
	def __init__(self, clientId):

        	# observer
        	self.observer = OrdersObserver()
        	self.tws_conn = Connection.create(port=7496, clientId=clientId)
        	self.tws_conn.connect()

        	# contract
		self.contract = Contract()
		self.contract.m_secType = 'STK'
		self.contract.m_exchange = 'SMART'
		self.contract.m_strike = 0.0

       		# order
		self.order = Order()
		self.order.m_account = "DU205019"
		self.order.m_orderType = 'MKT'
		self.order.m_auxPrice = 0.0
		self.order.m_tif = "DTC"

		self.msg_status = None

    	def close_connection(self):
            self.tws_conn.disconnect()
        
	def error_handler(self, msg):
		print "Server Error: %s"%msg

	def reply_handler(self, msg):
        	if msg.typeName in ('orderStatus','commissionReport'):
			if msg.typeName == 'orderStatus':
				self.msg_status = msg
		                broker_fee, fee_currency = 0, '0'
			  	print "Server Response: %s, %s, %s, %s, %s, %s, %s, %s, %s"%(msg.typeName, msg.orderId, \
        	    			msg.status, msg.filled, msg.remaining, msg.avgFillPrice, msg.permId, msg.lastFillPrice, msg.clientId)

	            	elif msg.typeName == 'commissionReport':
	                	print "commissionReport: %s, %s, %s"%(msg.commissionReport.m_commission, msg.commissionReport.m_currency, msg.commissionReport.m_execId)
	                    	broker_fee, fee_currency = msg.commissionReport.m_commission, msg.commissionReport.m_currency
                    
                	self.observer.get_orders_status(self.msg_status.orderId, self.msg_status.status, self.msg_status.filled, \
                                                	self.msg_status.remaining, self.msg_status.avgFillPrice, \
                                                	self.msg_status.permId, self.msg_status.lastFillPrice, self.msg_status.clientId, \
                                                	broker_fee, fee_currency)
                	self.observer.add_to_DB()

        	else:
            		pass
        	print "Server complete message: %s"%msg


	def create_order(self, stock_name, quantity, market_price=1, exchange_code='ASX', currency_code='AUD', action='BUY'):
    		"""Create an Order object (Market/Limit) to go long/short.

    		order_type - 'MKT', 'LMT' for Market or Limit orders
    		quantity - Integral number of assets to order
    		action - 'BUY' or 'SELL'"""
    		self.contract.m_symbol = str(stock_name)
    		self.contract.m_primaryExch = str(exchange_code)
    		self.contract.m_currency = str(currency_code)
	    	self.order.m_totalQuantity = int(quantity)
    		self.order.m_action = str(action)
    		self.order.m_lmtPrice = float(market_price)


    	def place_order(self, order_id=1):
	    	print 'place Order', order_id
 	        self.tws_conn.register(self.error_handler, 'Error')
            	self.tws_conn.registerAll(self.reply_handler)
            	self.tws_conn.placeOrder(order_id, self.contract, self.order)
	    	#tws_conn.disconnect()


class OrdersObserver:
    def __init__(self):
        pass

    def get_orders_status(self, orderId, status, filled, remaining, avgFillPrice, permId,\
                         lastFillPrice, clientId, brocker_fee=0, brocker_currency='0'):
        self.orderId, self.status, self.filled, self.remaining, self.avgFillPrice, self.permId, \
            self.lastFillPrice, self.clientId, self.brocker_fee, self.brocker_currency = orderId, status, filled, remaining, \
                    avgFillPrice, permId,lastFillPrice, clientId, brocker_fee, brocker_currency


    def add_to_DB(self):
        try:
            date_updated = datetime.now()
            db_order_status = StocksOrdersStatus(
                                order_status = self.status,
                                num_filled = self.filled,
                                num_remaining = self.remaining,
                                avg_fill_price = self.avgFillPrice,
                                tws_id = self.permId,
                                last_fill_price = self.lastFillPrice, 
                                date_updated = date_updated)

            order_id = int(self.orderId) 
            db_order_status.order = UserStocksOrders.objects.get(id =self.orderId)
                
                
            if (StocksOrdersStatus.objects.filter(order_id =self.orderId, order_status= self.status).exists()):
                pass
            elif (StocksOrdersStatus.objects.filter(order_id =self.orderId, order_status= 'PendingSubmit').exists()):
                temp_order = StocksOrdersStatus.objects.get(order_id =self.orderId, order_status= 'PendingSubmit')
                temp_order.order_status = self.status
                temp_order.num_filled = self.filled
                temp_order.num_remaining = self.remaining
                temp_order.avg_fill_price = self.avgFillPrice
                temp_order.tws_id = self.permId
                temp_order.last_fill_price = self.lastFillPrice
                temp_order.date_updated = date_updated
                temp_order.save()
            else:
                db_order_status.save()

            # add commission

            if (self.brocker_currency != '0'):
                db_commission_fee = CommissionFee(
                                    order = db_order_status.order,
                                    brocker_fee = self.brocker_fee, 
                                    brocker_currency = self.brocker_currency,
                                    macrovue_fee = 0,
                                    macrovue_currency = 'AUD')
                    
                db_commission_fee.save()

            # add completed_orders to user_stocks_history table
            db_order_status = StocksOrdersStatus.objects.get(order_id =self.orderId, order_status= 'Filled')
            if db_order_status:
                print db_order_status
                print db_order_status.order_status
                print db_order_status.date_updated.date()
                print type(db_order_status.date_updated.date())

                db_user_stock = UserStocksOrders.objects.get(id = self.orderId)
                stock_id = db_user_stock.stock.id
                user_id = db_user_stock.user.id
                print stock_id, user_id
                if db_user_stock.buy_sell:
                    # True means Buy
                    price_buy, num_buy = db_order_status.last_fill_price, db_order_status.num_filled
                    price_sell, num_sell = 0, 0
                else:
                    price_sell, num_sell = db_order_status.last_fill_price, db_order_status.num_filled
                    price_buy, num_buy = 0, 0

                date_buy = date_sell = db_order_status.date_updated.date()

                db_user_stocks_history =  UserStocksHistory(date_buy = date_buy,
                                                            price_buy = price_buy,
                                                            num_buy = num_buy,
                                                            date_sell = date_sell,
                                                            price_sell = price_sell,
                                                            num_sell = num_sell)

                db_user_stocks_history.user, db_user_stocks_history.stock = db_user_stock.user, db_user_stock.stock

                db_user_stocks_history.save()

        except:
            print "there is a problem in order_manager.py in response thread"
