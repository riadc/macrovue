from django.contrib import admin

from vues.models import Vues, Stocks, StockFinancialData, VueStocks, Menu, VueMenus, VueFinancialData, VueReturns, VueStocks, UserPortfolioValue
from home.models import Questions, Categories, CategoryQuestions

class CategoryQuestionsAdmin(admin.ModelAdmin):
	fields=['category','question']
	list_display=('category','question')

admin.site.register(Vues)
admin.site.register(Stocks)
admin.site.register(StockFinancialData)

admin.site.register(VueStocks)
admin.site.register(Menu)
admin.site.register(VueMenus)
admin.site.register(VueFinancialData)
admin.site.register(VueReturns)
admin.site.register(Questions)
admin.site.register(Categories)
admin.site.register(CategoryQuestions,CategoryQuestionsAdmin)
admin.site.register(UserPortfolioValue)
