from django import template
from django.conf import settings

register = template.Library()

@register.assignment_tag
def keyvalue(dict, key):
  try:
    return dict[key]
  except KeyError:
    return ''

@register.simple_tag
def settings_value(name):
  return getattr(settings, name, "")
