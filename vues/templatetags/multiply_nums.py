from django import template
register = template.Library()

@register.filter(name='multiply')
def multiply(numOne, numTwo, *args, **kwargs):
    return numOne * numTwo