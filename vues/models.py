from django.db import models
from django.contrib.auth.models import User

class Menu(models.Model):
	name = models.CharField(max_length=30)
	type = models.IntegerField(default=1)
	parent = models.ForeignKey('self', null=True, blank=True)
	def __str__(self):
		if self.type == 1:
			return ">"+self.name
		elif self.type == 2:
			return ">>"+self.name
		elif self.type == 3:
			return ">>>"+self.name
		else:
			return "----"+self.name
	class Meta:
		db_table = "menu"

class Index(models.Model):
	name = models.CharField(max_length=20)

	class Meta:
		db_table = "index"

	def __str__(self):
		return self.name

class CurrencyCode(models.Model):
	currency = models.CharField(max_length=25, null=False)
	code = models.CharField(max_length=15, null=False)

	class Meta:
		db_table = "currency_code"
		verbose_name_plural = "currency_codes"


class ExchangeCode(models.Model):
	name = models.CharField(max_length=30, null=False)
	code = models.CharField(max_length=15, null=False)
	ib_code = models.CharField(max_length=15, null=True)

	class Meta:
		db_table = "exchange_code"
		verbose_name_plural = "exchange_codes"


# Create your models here.
class Stocks(models.Model):
	factset_id = models.CharField(max_length=20,default='')
	macrovue_id = models.CharField(max_length=40,default='')
	index=models.ForeignKey(Index,null=False)
	industry = models.CharField(max_length=40,default='')
	sector = models.CharField(max_length=40,default='')
	cusip=models.CharField(max_length=20,default='')
	sedol=models.CharField(max_length=20,default='')
	isin=models.CharField(max_length=20,default='')
	exchange = models.CharField(max_length=40,default='')
	exchange_code = models.ForeignKey(ExchangeCode, null=True)
	country = models.CharField(max_length=40,default='')
	currency = models.CharField(max_length=20,default='')
	currency_code = models.ForeignKey(CurrencyCode, null=True)
	name = models.CharField(max_length=120,default='')
	ticker = models.CharField(max_length=10,default='')
	def __str__(self):
		return self.macrovue_id
	class Meta:
		db_table = "stocks"
		verbose_name_plural = "Stocks"

# the financial data for a Stocks
class StockFinancialData(models.Model):
	stock = models.ForeignKey(Stocks, null=True)
	date = models.DateField(auto_now = False, auto_now_add = False)
	dividend_yield = models.DecimalField(max_digits = 35, decimal_places = 7)
	price_equity_ratio = models.BigIntegerField(default = '0')
	mkt_cap = models.BigIntegerField(default = 0)
	one_year_vol = models.DecimalField(max_digits = 10, decimal_places = 7, default='0')
	one_mo_return = models.DecimalField(max_digits = 15, decimal_places = 7)
	one_yr_return = models.DecimalField(max_digits = 15, decimal_places = 7)
	closing_price = models.DecimalField(max_digits = 15, decimal_places = 7, default='0')
	valuation = models.IntegerField(default = 0) 
	growth = models.IntegerField(default = 0)
	volatility = models.IntegerField(default = 0)

	class Meta:
		db_table = "stock_financial_data"
		verbose_name_plural = "StockFinancialData"

class Vues(models.Model):
	name = models.CharField(max_length=30)
	user_generated = models.BooleanField(default=False)
	description = models.CharField(max_length=1000, default='')
	short_description = models.CharField(max_length=200, default='')
	analysis = models.TextField(null=True)
	user = models.ForeignKey(User,null=True)
	stocks = models.ManyToManyField(Stocks, through='VueStocks')
	country = models.CharField(max_length=30,default='U.S')
	created_at = models.DateField(null=True)
	updated_at = models.DateField(null=True)
	menu_option = models.ManyToManyField(Menu, through='VueMenus')
	background = models.CharField(max_length = 150, default='http://127.0.0.1:8000/static/images/macrovue2/techstars.jpg')
	votes = models.ManyToManyField(User, through='VueVotes', related_name='votes')
	is_visible = models.BooleanField(default=True)
	index = models.ForeignKey(Index)
	custom_vue = models.ForeignKey('self',null=True,blank=True)
	def __str__(self):
		return self.name
	class Meta:
		db_table = "vues"
		verbose_name_plural = "Vues"

class VueVotes(models.Model):
	user = models.ForeignKey(User)
	vue = models.ForeignKey(Vues)
	type = models.BooleanField(default=True)		
	class Meta:
		db_table = "vue_votes"

# linking a Vues to a Stocks
class VueStocks(models.Model):
	vue = models.ForeignKey(Vues, null=True)
	stock = models.ForeignKey(Stocks, null=True)
	def __str__(self):
		return str(self.stock) + " - " + str(self.vue)
	class Meta:
		db_table = "vue_stocks"
		verbose_name_plural = "VueStocks"

class IndexReturns(models.Model):
	stock = models.ForeignKey(Stocks, null=True)

	def __str__(self):
		return str(self.stock) + " - " + str(self.vue)
	class Meta:
		db_table = "IndexReturns"
		verbose_name_plural = "VueStocks"

class VueReturns(models.Model):
	vue = models.ForeignKey(Vues)
	date = models.DateField(auto_now = False, auto_now_add = False)
	daily_return=models.DecimalField(max_digits = 22, decimal_places = 20)
	cum_return=models.DecimalField(max_digits = 22, decimal_places = 20)
	index_return=models.DecimalField(max_digits = 22, decimal_places = 20)
	class Meta:
		db_table = "vue_returns"
		verbose_name_plural = "VueReturns"

class VueMenus(models.Model):
	menu = models.ForeignKey(Menu)
	vue = models.ForeignKey(Vues)
	def __str__(self):
		return str(self.menu) + " - " + str(self.vue)
	class Meta:
		db_table = "vue_menus"
		verbose_name_plural = "VueMenus"	

class VueFinancialData(models.Model):
	vue = models.ForeignKey(Vues)
	date = models.DateField(auto_now = False, auto_now_add = False)
	dividend_yield = models.DecimalField(max_digits = 4, decimal_places = 2)
	valuation = models.IntegerField(default = 0)
	growth = models.IntegerField(default = 0)
	volatility = models.IntegerField(default = 0)
	one_mo_return = models.DecimalField(max_digits = 4, decimal_places = 2)

	class Meta:
		db_table = "vue_financial_data"
		verbose_name_plural = "VueFinancialData"		

class UserActivated(models.Model):
	user = models.OneToOneField(User)
	isActivated = models.BooleanField(default=False)
	randomText = models.CharField(max_length=40)
	class Meta:
		db_table = "user_activated"			

class UserFavourites(models.Model):
	user = models.ForeignKey(User,null=False)
	vue = models.ForeignKey(Vues,null=False)
	def __str__(self):
		return str(self.user) + " - " + str(self.vue)
		
	class Meta:
		db_table = "user_favourites"			

class VueRating(models.Model):
	user = models.ForeignKey(User,null=False)
	vue = models.ForeignKey(Vues,null=False)
	rating = models.IntegerField(default = 0)
	class Meta:
		db_table = "vue_ratings"

class UserPortfolioValue(models.Model):
    user = models.ForeignKey(User,null=False)
    date = models.DateField()
    total_value = models.DecimalField(max_digits = 15, decimal_places = 2, default='0')
    cash_value = models.DecimalField(max_digits = 15, decimal_places = 2, default='0')
    position_value = models.DecimalField(max_digits = 15, decimal_places = 2, default='0')
    def __str__(self):
		return str(self.user) + " - " + str(self.date)
    class Meta:
		db_table = "user_portfolio_value"
		verbose_name_plural = "user_portfolio_value"

class UserVueValue(models.Model):
    user = models.ForeignKey(User,null=False)
    date = models.DateField()
    vue = models.ForeignKey(Vues,null=False)    
    date_buy= models.DateField()
    price_buy= models.DecimalField(max_digits = 15, decimal_places = 2)    
    vue_value = models.DecimalField(max_digits = 15, decimal_places = 2, default='0')
    vue_value_change = models.DecimalField(max_digits = 15, decimal_places = 2, default='0')
    class Meta:
     	db_table = "user_vue_value"
     	verbose_name_plural = "user_vue_value"


class UserStockValue(models.Model):
    user = models.ForeignKey(User,null=False)
    date = models.DateField()
    stock_id = models.ForeignKey(Stocks,null=False)    
    date_buy= models.DateField()
    price_buy= models.DecimalField(max_digits = 15, decimal_places = 2)  
    stock_value = models.DecimalField(max_digits = 15, decimal_places = 2, default='0')
    stock_value_change = models.DecimalField(max_digits = 15, decimal_places = 2, default='0')
    class Meta:
		db_table = "user_stock_value"
		verbose_name_plural = "user_stock_value"

class UserVuesHistory(models.Model):
    user = models.ForeignKey(User,null=False)
    vue = models.ForeignKey(Vues,null=False)
    date_buy= models.DateField()
    price_buy= models.DecimalField(max_digits = 15, decimal_places = 2)
    date_sell= models.DateField()
    price_sell= models.DecimalField(max_digits = 15, decimal_places = 2)
    class Meta:
    	db_table = "user_vues_history"
    	verbose_name_plural = "user_vues_history"

class UserStocksHistory(models.Model):
    user = models.ForeignKey(User,null=False)
    stock = models.ForeignKey(Stocks,null=False)
    date_buy = models.DateField(null=True)
    price_buy = models.DecimalField(max_digits = 15, decimal_places = 2)
    num_buy = models.IntegerField()
    date_sell= models.DateField(null=True)
    price_sell= models.DecimalField(max_digits = 15, decimal_places = 2)
    num_sell = models.IntegerField()
    class Meta:
    	db_table = "user_stocks_history"
    	verbose_name_plural = "user_stocks_history"

class UserStocksOrders(models.Model):
	user = models.ForeignKey(User,null=False)
	date_created = models.DateTimeField()
	stock = models.ForeignKey(Stocks,null=False)
	quantity = models.IntegerField()
	price = models.DecimalField(max_digits = 15, decimal_places = 2)
	vue = models.ForeignKey(Vues, null=False)
	buy_sell = models.BooleanField(default=True) # True: Buy, False: Sell
	class Meta:
		db_table = "user_stocks_order"
		verbose_name_plural = "user_stocks_order"

class StocksOrdersStatus(models.Model):
	order = models.ForeignKey(UserStocksOrders, null=False)
	order_status = models.CharField(max_length=30)
	num_filled = models.IntegerField()
	num_remaining = models.IntegerField()
	avg_fill_price = models.DecimalField(max_digits = 15, decimal_places = 2)
	tws_id = models.IntegerField()
	last_fill_price = models.DecimalField(max_digits = 15, decimal_places = 2)
	date_updated = models.DateTimeField()

	class Meta:
		db_table = "stocks_orders_status"
		verbose_name_plural = "stocks_orders_status"

class CommissionFee(models.Model):
	order = models.ForeignKey(UserStocksOrders, null=False)
	brocker_fee = models.DecimalField(max_digits = 10, decimal_places = 2)
	brocker_currency = models.CharField(max_length=15)
	macrovue_fee = models.DecimalField(max_digits = 10, decimal_places = 2)
	macrovue_currency = models.CharField(max_length=15)

	class Meta:
		db_table = "commission_fee"
		verbose_name_plural = "commission_fees"

class StockReturnData(models.Model):
	stock = models.ForeignKey(Stocks)
	date = models.DateField(auto_now = False, auto_now_add = False)
	#closing prices from factset
	closing_price_aud = models.DecimalField(max_digits = 15, decimal_places = 7, null=True)
	closing_price_local = models.DecimalField(max_digits = 15, decimal_places = 7, null=True)
    #Local data that  
	capital_gain_local=models.DecimalField(max_digits = 15, decimal_places = 7,null=True)
	dividend_local = models.DecimalField(max_digits = 15, decimal_places = 7, null=True)
    #Dollar change data that is displayed 
	dividend_aud = models.DecimalField(max_digits = 15, decimal_places = 7,null=True)
	capital_gain_aud=models.DecimalField(max_digits = 15, decimal_places = 7,null=True)
	currency_gain_aud=models.DecimalField(max_digits = 15, decimal_places = 7,null=True)
	total_gain_aud=models.DecimalField(max_digits = 15, decimal_places = 7,null=True)
    #Percent change data that is displayed
	capital_gain_percent=models.DecimalField(max_digits = 15, decimal_places = 7,null=True)
	dividend_gain_percent=models.DecimalField(max_digits = 15, decimal_places = 7,null=True)
	currency_gain_percent=models.DecimalField(max_digits = 15, decimal_places = 7,null=True)
	total_return_percent=models.DecimalField(max_digits = 15, decimal_places = 7,null=True)

	class Meta:
		db_table = "stock_return_data"
		verbose_name_plural = "StockReturnData"