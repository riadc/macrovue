from django.views.decorators.csrf import csrf_exempt

from datetime import timedelta, date

from dateutil.relativedelta import relativedelta

from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from vues.models import Vues, Stocks, VueStocks, Menu, VueMenus, VueReturns, StockFinancialData, \
						VueFinancialData, UserActivated, UserFavourites, VueRating, UserPortfolioValue, \
						UserVueValue, UserStockValue, UserStocksHistory, UserVuesHistory, Index, StockReturnData

from vues.models import UserStocksOrders, StocksOrdersStatus, CurrencyCode, ExchangeCode

from django.db.models import Q, Sum
from django.db import connection
from users.models import User
from vues.templatetags import app_filters
from home.utils import get_mailchimp_api, get_subscription_list_id
from home.models import SubscribedUser

import mailchimp, mandrill, os, datetime
#from datetime import datetime

from vues.order import order_manager

def show(request, vue_name):
	# get the stock without the hyphen if it has it
	if (" " in vue_name):
		return HttpResponseRedirect("/vues/"+vue_name.replace(" ","-"))

	stock_new = vue_name.replace("-", " ")
	# grab the vue
	vue = Vues.objects.get(name=stock_new)
	
	# if not visible, DONT SHOW THE VUE
	
	# grab the vue financial data
	vue_financial_data = VueFinancialData.objects.filter(vue_id=vue.id).order_by('-date')[0]

	# set the breadcrumbs
	breadcrumbs = {1 : 'Existing Vues' , 'url1' : 'existing_vues', 2 : stock_new, 'url2' : 'index'}
	#omr = one month return
	one_month_return = vue_financial_data.one_mo_return
	one_mo_return_decimal = str(one_month_return-int(one_month_return)).split('.')[1]
	dividend_yield_decimal = str(one_month_return-int(vue_financial_data.dividend_yield)).split('.')[1]
	user_creator = User.objects.get(id=vue.user_id)
	
	# your rating
	if (request.user.is_authenticated()):
		if (VueRating.objects.filter(user_id = request.user.id, vue_id = vue.id).exists()):
			your_rating	= VueRating.objects.get(user_id = request.user.id, vue_id = vue.id)
		else:
			your_rating	= VueRating.objects.create(user_id = request.user.id, vue_id = vue.id)
	else:
		your_rating = 0
	# everyone elses rating
	all_ratings = VueRating.objects.filter(vue_id = vue.id)
	# the count
	count = all_ratings.count()
	avg_rating = 0;
	
	for i in all_ratings:
		avg_rating += i.rating

	avg_final = (avg_rating / count) if count !=0 else 0
	
	has_favourited = False
	if (UserFavourites.objects.filter(user_id=request.user.id, vue_id=vue.id).exists()):
		has_favourited = True
		
	# 5 total queries
	# grab the asset infos (all of it)
	stockinfos = VueStocks.objects.filter(vue_id = vue.id).values_list('stock_id', flat=True)
	# connect it to the assets
	stocks = Stocks.objects.filter(id__in = stockinfos).exclude(exchange_code_id__isnull=True)
	# now grab the variable data
	date = StockFinancialData.objects.values('date').distinct().order_by('-date')[:1]
	stockdata = StockFinancialData.objects.filter(stock_id__in = stockinfos,date=date)

	return render_to_response('vues/show.html', locals(), context_instance=RequestContext(request))

def list(request):
	# get the list of vues
	vues = Vues.objects.filter(is_visible=True,custom_vue__isnull=True)
	# then grab the date
	date = VueFinancialData.objects.filter(vue__custom_vue__isnull=True,vue__is_visible=True).values('date').distinct().order_by('-date')[:1]

	vue_financial_data = VueFinancialData.objects.filter(date=date)
	page_amt = 9
		
	# find the parents
	parents = Menu.objects.filter(type=1)
	# find the categories
	categories = Menu.objects.filter(type=2)
	
	return render_to_response('vues/list.html', locals(), context_instance=RequestContext(request))

def users_vues(request):
	# get the list of vues
	vues = Vues.objects.filter(is_visible=True,custom_vue__isnull=False)
	# then grab the date
	vue_financial_data = VueFinancialData.objects.filter(vue__custom_vue__isnull=False).order_by('date')
	page_amt = 9
		
	# find the parents
	parent = Menu.objects.get(name='Location')
	# find the categories
	categories = Menu.objects.filter(parent=parent)
	
	return render_to_response('vues/list_uservues.html', locals(), context_instance=RequestContext(request))

@login_required
def customise(request, vue_name):
	# same as the show view but for customising
	stock_new = vue_name.replace("-", " ")
		
	vue = Vues.objects.get(name=stock_new)

	if (Vues.objects.filter(custom_vue=vue.id, user=request.user.id).exists()):
		vue = Vues.objects.get(custom_vue=vue.id, user=request.user.id)

	# grab the asset infos (all of it)
	stockinfos = VueStocks.objects.filter(vue_id = vue.id).values_list('stock_id', flat=True)
	# connect it to the assets
	stocks = Stocks.objects.filter(id__in = stockinfos)
	# now grab the variable data
	stockdata = StockFinancialData.objects.filter(stock_id__in = stockinfos)

	breadcrumbs = {1 : 'Existing Vues' , 'url1' : 'existing_vues', 2 : vue_name, 'url2' : 'existing_vues', 3 : 'customise'}

	return render_to_response('vues/customise.html', locals(), context_instance=RequestContext(request))

@login_required
def create_vue(request):
	indexes = Index.objects.all()
	return render_to_response('vues/createvue.html', locals(), context_instance=RequestContext(request))

@login_required
def save_new_vue(request):
	vue_name = request.POST['vue_name']
	vue_description = request.POST['vue_description']
	vue_short_description = request.POST['vue_short_description']
	vue_privacy = request.POST.get('vue_privacy', False)
	vue_country = request.POST['vue_country']
	vue_index = Index.objects.get(id=request.POST['vue_index'])
	stock_list = request.POST['stocklist'].split(',')
	
	if (vue_privacy == 'on'):
		privacy = True
	else:
		privacy = False
	
	if (Vues.objects.filter(name=vue_name).exists()):
		return HttpResponse('Vue with this name already exists, please modify it.')
	else:
		Vues.objects.create(name=vue_name,user_generated=True,user=request.user,description=vue_description, short_description=vue_short_description,is_visible=vue_privacy,country=vue_country,index_id=vue_index)
	
	vue = Vues.objects.get(name=vue_name)
	vue.custom_vue = vue
	vue.updated_at = datetime.datetime.today()
	vue.created_at = datetime.datetime.today()

	vue.save()

	valuation = 0
	growth = 0
	volatility = 0
	one_mo_return = 0
	dividend_yield = 0
	i = 0;
	date = StockFinancialData.objects.values('date').distinct().order_by('-date')[:1]
	
	for stock in stock_list:
		i += 1
		stock_info = Stocks.objects.get(id=stock)
		sf = StockFinancialData.objects.get(stock_id = stock_info.id, date=date)
		valuation += sf.valuation
		growth += sf.growth
		volatility += sf.volatility
		one_mo_return += sf.one_mo_return
		dividend_yield += sf.dividend_yield
		VueStocks.objects.create(stock=stock_info,vue=vue)
		
	valuation = valuation/i
	growth = growth/i
	volatility = volatility/i
	one_mo_return = one_mo_return/i
	dividend_yield = dividend_yield/i
	
	VueFinancialData.objects.create(vue=vue,date=datetime.datetime.today(),dividend_yield=dividend_yield,one_mo_return=one_mo_return,growth=growth,valuation=valuation,volatility=volatility)
		
	return HttpResponseRedirect("/vues/"+vue_name)
	
def save_custom(request):
	# grab stock list
	stock_list = request.POST['stocklist'].split(',')
	# grab vue id
	vue_id = request.POST['vueid']
	privacy = request.POST.get('privacy', False)
	# grab the vue
	vue = Vues.objects.get(id = vue_id)	
	
	# if the user owns this vue
	if (request.user.id == vue.user.id):
		custom = Vues.objects.get(id=vue.id, user=request.user.id)
		VueStocks.objects.filter(vue=custom).delete()
		custom.is_visible = False;
		custom.updated_at = datetime.datetime.today()
		if (privacy == 'on'):
			custom.is_visible = True
		custom.save()
		
		valuation = 0
		growth = 0
		volatility = 0
		one_mo_return = 0
		dividend_yield = 0
		i = 0;
		date = StockFinancialData.objects.values('date').distinct().order_by('-date')[:1]
		
		for stock in stock_list:
			i += 1
			stock_info = Stocks.objects.get(id=stock)
			sf = StockFinancialData.objects.get(stock_id = stock_info.id, date=date)
			valuation += sf.valuation
			growth += sf.growth
			volatility += sf.volatility
			one_mo_return += sf.one_mo_return
			dividend_yield += sf.dividend_yield
			VueStocks.objects.create(stock=stock_info,vue=custom)
			
		valuation = valuation/i
		growth = growth/i
		volatility = volatility/i
		one_mo_return = one_mo_return/i
		dividend_yield = dividend_yield/i

		if (VueFinancialData.objects.filter(vue=custom,date=datetime.datetime.today()).exists()):
			VueFinancialData.objects.get(vue=custom,date=datetime.datetime.today()).delete()
		
		VueFinancialData.objects.create(vue=custom,date=datetime.datetime.today(),dividend_yield=dividend_yield,one_mo_return=one_mo_return,growth=growth,valuation=valuation,volatility=volatility)
		
	# oh no! they don't own it... Well, is it visible?
	else:
		if (vue.is_visible == True):
			# create the new custom vue
			custom = vue
			custom.name = str(request.user).title()+" "+vue.name
			custom.user_generated = True
			custom.custom_vue = vue
			custom.pk = None
			custom.user = request.user
			custom.is_visible = False;
			custom.updated_at = datetime.datetime.today()
			custom.created_at = datetime.datetime.today()

			if (privacy == 'on'):
				custom.is_visible = True
			custom.save()
			# delete all old stocks
			VueStocks.objects.filter(vue=custom).delete()

			valuation = 0
			growth = 0
			volatility = 0
			one_mo_return = 0
			dividend_yield = 0
			i = 0;		
			date = StockFinancialData.objects.values('date').distinct().order_by('-date')[:1]

			# add the new ones
			for stock in stock_list:
				i += 1
				stock_info = Stocks.objects.get(id=stock)
				sf = StockFinancialData.objects.get(stock_id = stock_info.id, date=date)
				valuation += sf.valuation
				growth += sf.growth
				volatility += sf.volatility
				one_mo_return += sf.one_mo_return
				dividend_yield += sf.dividend_yield
				VueStocks.objects.create(stock=stock_info,vue=custom)

			valuation = valuation/i
			growth = growth/i
			volatility = volatility/i
			one_mo_return = one_mo_return/i
			dividend_yield = dividend_yield/i

			if (VueFinancialData.objects.filter(vue=custom,date=datetime.datetime.today()).exists()):
				VueFinancialData.objects.get(vue=custom,date=datetime.datetime.today()).delete()

			VueFinancialData.objects.create(vue=custom,date=datetime.datetime.today(),dividend_yield=dividend_yield,one_mo_return=one_mo_return,growth=growth,valuation=valuation,volatility=volatility)

		else:
			return HttpResponseRedirect("/existing_vues")

	return HttpResponseRedirect("/vues/"+custom.name.replace(" ","-")+"/customise")
    
def getstocks(request, ticker_name, index):
	# used for ajax getting of stocks
	stocks = Stocks.objects.filter(Q(index=index) & (Q(ticker__icontains = ticker_name) | Q(name__icontains = ticker_name)));
	return render_to_response('vues/getstocks.html', locals(), context_instance=RequestContext(request), content_type="application/json")

def getstockfinancials(request, ticker_id):
	stockdata = StockFinancialData.objects.get(stock_id = ticker_id);
	return render_to_response('vues/getstockfinancials.html', locals(), context_instance=RequestContext(request), content_type="application/json")

def getvues(request, menu):
	# used for ajax getting of vues	
	if (menu == "0"):
		vue_financial = VueFinancialData.objects.filter(vue__custom_vue__isnull=True,vue__is_visible=True).values('date').distinct().order_by('-date')[:1]
		vue_financial_data = VueFinancialData.objects.filter(date=vue_financial)
		vues_list = Vues.objects.filter(is_visible = True)
	else:
		menu_vues_list = VueMenus.objects.filter(menu_id = menu).values_list('vue_id', flat=True)
		vue_financial = VueFinancialData.objects.filter(vue__custom_vue__isnull=True,vue__is_visible=True).values('date').distinct().order_by('-date')[:1]
		vue_financial_data = VueFinancialData.objects.filter(date=vue_financial, vue_id__in = menu_vues_list)
		# connect it to the assets
		vues_list = Vues.objects.filter(id__in = menu_vues_list, is_visible = True)
	return render_to_response('vues/getvues.html', locals(), context_instance=RequestContext(request), content_type="application/json")

def getvuereturns(request, vue):
	# used for ajax getting of vue return data
	if "-index" in vue:
		vue = vue.replace('-index','')
		success = True
	else:
		success = False

	vue_info = Vues.objects.get(id=vue)
	vue_return_info = VueReturns.objects.filter(vue_id=vue,date__gte=vue_info.created_at)
	
	return render_to_response('vues/getvuereturns.html', locals(), context_instance=RequestContext(request), content_type="application/json")

@login_required
def submit_rating(request, vue_id, rating):
	if (rating > "5" or rating < "0"):
		return HttpResponse('False')
		
	# submit rating for a vue
	if (VueRating.objects.filter(vue_id=vue_id, user_id=request.user.id).exists()):
		VueRating.objects.get(vue_id=vue_id,user_id=request.user.id).delete()

	VueRating.objects.create(user_id=request.user.id,vue_id=vue_id,rating=rating)
	return HttpResponse('True')

def index(request):
	stock_names = Vues.objects.filter(is_visible=True)
	stocks_info = []
	i = 0
	for stock_id, stock_name in enumerate(stock_names):
		if (i > 5):
			break

		i = i+1

		try:
			# grab the vue
  			vue = Vues.objects.get(name=stock_name)
  			# grab the vue financial data
  			vue_financial_data = VueFinancialData.objects.filter(vue_id=vue.id).order_by('-date')[0]

			if((i-1) == 4):
				stocks_info.remove({'stock_name':str(stock_name).replace(" ", "-"), 
	  			             	   'one_month_return': round(vue_financial_data.one_mo_return,1),
	  		    	        	    'url': '/vues/'+str(stock_name).replace(" ", "-")+'/',
	  		    	       		    'id': 'slider-'+str(stock_id+2),
	  		    	        	    'short_descr': vue.short_description})



  			#omr = one month return
	  		stocks_info.append({'stock_name':str(stock_name).replace(" ", "-"), 
	  			                'one_month_return': round(vue_financial_data.one_mo_return,1),
	  		    	            'url': '/vues/'+str(stock_name).replace(" ", "-")+'/',
	  		    	            'id': 'slider-'+str(stock_id+2),
	  		    	            'short_descr': vue.short_description})
  		except:
  			pass
	# the index is now here... if user not signed in show this page

	return render_to_response('home/index.html', locals(), context_instance=RequestContext(request))

def subscribe(request):
	try:
		m = get_mailchimp_api()
		list_id = get_subscription_list_id(m)
		email = request.POST['email']
		m.lists.subscribe(list_id, {'email':email})
		SubscribedUser.objects.get_or_create(email=email)
	except mailchimp.ListAlreadySubscribedError:
		return HttpResponse('That email is already subscribed to the list')
	except mailchimp.Error, e:
		return HttpResponse('An error occurred: %s - %s' % (e.__class__, e))
	return HttpResponse('')

@login_required
def favourites(request):
	#Temporary render, just to get layout and structure.
	favourites = UserFavourites.objects.filter(user_id=request.user.id).values_list('vue_id', flat=True)
	favourites_info = Vues.objects.filter(id__in = favourites)
	favourites_fin = VueFinancialData.objects.filter(vue_id__in = favourites).order_by('-date')
		
	return render_to_response('vues/favourites.html', locals(), context_instance=RequestContext(request))

@login_required
def customvues(request):
	custom_list = Vues.objects.filter(user_id=request.user.id).values_list('id',flat=True)
	custom_vues = Vues.objects.filter(id__in=custom_list)
	custom_fin = VueFinancialData.objects.filter(vue_id__in = custom_list).order_by('-date')

	return render_to_response('vues/myvues.html', locals(), context_instance=RequestContext(request))

# class PercentagePortfolio:
# 	def __init__(self, stock_id, total_return_percent, currency_gain_percent, dividends_gain_percent, capital_gain_percent):
# 		self.stock_id = stock_id
# 		self.total_return_percent = total_return_percent
# 		self.currency_gain_percent = currency_gain_percent
# 		self.dividends_gain_percent = dividends_gain_percent
# 		self.capital_gain_percent = capital_gain_percent

#Portfolio 
@login_required
def portfolio(request):
	#take the most recent user portfolio data to display
	try:
		portfolio_value = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('-date')[0]
	except:
		portfolio_value = 0

	# # #quantity
	user_stocks_history = UserStocksHistory.objects.filter(user_id=request.user.id)
	# #name, price
	stocks = Stocks.objects.filter(id__in=user_stocks_history.values('stock_id')).order_by('id')


	# #all 
	stock_return_data_all = StockReturnData.objects.filter(stock_id__in=user_stocks_history.values('stock_id')).order_by('stock')
	all_data = StockReturnData.objects.filter(stock_id__in=user_stocks_history.values('stock_id')).values('stock').annotate(total_gain_aud=Sum('total_gain_aud')).annotate(dividends_local=Sum('dividend_local')).annotate(capital_gain_local=Sum('capital_gain_local')).annotate(currency_gain_aud=Sum('currency_gain_aud')).order_by('stock')
	# #1 week
	# week = datetime.date.today() - relativedelta(days=7)
	# week_return_data = StockReturnData.objects.filter(stock_id__in=user_stocks_history.values('stock_id')).filter(Q(date__gte=week)).values('stock_id').annotate(total_gain_aud=Sum('total_gain_aud')).annotate(dividend_local=Sum('dividends_local')).annotate(capital_gain_local=Sum('capital_gain_local')).annotate(currency_gain_aud=Sum('currency_gain_aud')).order_by('stock_id')

	# #1 month
	# one_month = datetime.date.today() - relativedelta(months=1)
	# month_return_data = StockReturnData.objects.filter(stock_id__in=user_stocks_history.values('stock_id')).filter(Q(date__gte=one_month)).values('stock_id').annotate(total_gain_aud=Sum('total_gain_aud')).annotate(dividend_local=Sum('dividends_local')).annotate(capital_gain_local=Sum('capital_gain_local')).annotate(currency_gain_aud=Sum('currency_gain_aud')).order_by('stock_id')

	# #3 months 
	# three_months = datetime.date.today() - relativedelta(months=3)
	# three_month_return_data = StockReturnData.objects.filter(stock_id__in=user_stocks_history.values('stock_id')).filter(Q(date__gte=three_months)).values('stock_id').annotate(total_gain_aud=Sum('total_gain_aud')).annotate(dividend_local=Sum('dividends_local')).annotate(capital_gain_local=Sum('capital_gain_local')).annotate(currency_gain_aud=Sum('currency_gain_aud')).order_by('stock_id')

	# #1 year
	# year = datetime.date.today() - relativedelta(years=1)
	# year_return_data = StockReturnData.objects.filter(stock_id__in=user_stocks_history.values('stock_id')).filter(Q(date__gte=year)).values('stock_id').annotate(total_gain_aud=Sum('total_gain_aud')).annotate(dividend_local=Sum('dividends_local')).annotate(capital_gain_local=Sum('capital_gain_local')).annotate(currency_gain_aud=Sum('currency_gain_aud')).order_by('stock_id')

	# #year to date
	# ytd = date(date.today().year, 1, 1)
	# ytd_return_data = StockReturnData.objects.filter(stock_id__in=user_stocks_history.values('stock_id')).filter(Q(date__gte=ytd)).values('stock_id').annotate(total_gain_aud=Sum('total_gain_aud')).annotate(dividend_local=Sum('dividends_local')).annotate(capital_gain_local=Sum('capital_gain_local')).annotate(currency_gain_aud=Sum('currency_gain_aud')).order_by('stock_id')

	# #financial year
	# if( date.today().month >=7 ):
	# 	curr_fin = date(date.today().year, 7, 1)
	# else: 
	# 	curr_fin = date(date.today().year -1 , 7, 1)

	# finyear_return_data = StockReturnData.objects.filter(stock_id__in=user_stocks_history.values('stock_id')).filter(Q(date__gte=curr_fin)).values('stock_id').annotate(total_gain_aud=Sum('total_gain_aud')).annotate(dividend_local=Sum('dividends_local')).annotate(capital_gain_local=Sum('capital_gain_local')).annotate(currency_gain_aud=Sum('currency_gain_aud'))

	# #stock_list - getting a matrix of all stock data, for easy frontend parsing
	stock_count = UserStocksHistory.objects.filter(user_id=request.user.id).count()
	# #   0        1     2           3         4        {     5            6               7         8            9             10              11           12 } -repeat
	# #stock_id, name, date_buy, quantity, current_val, [all_capital_$, all_captial_%, all_div_$, all_div_%, all_currency_$, all_currency_%, all_total_$, all_total_%,] .... 
	stock_list = [[0 for x in xrange(61)] for x in xrange(stock_count)]
	# #working on percentages and adding to the stock list 
	for i, uHis in enumerate(user_stocks_history):
	# 	#id, date_buy, quantity
	 	stock_list[i][0] = uHis.stock_id
	 	stock_list[i][2] = uHis.date_buy
	 	stock_list[i][3] = uHis.num_buy
	# 	#name
	 	for stock in stocks:
	 		if(stock.id == uHis.stock_id):
	 			stock_list[i][1] = stock.name
	# 	# $$$ all capital, all div, all currency, all total
	 	for obj in all_data:
	 		if(obj['stock'] == uHis.stock_id):
	 			stock_list[i][5] = obj['capital_gain_local']
	 			stock_list[i][7] = obj['dividends_local']
	 			stock_list[i][9] = obj['currency_gain_aud']
	 			stock_list[i][11] = obj['total_gain_aud']

	# 	#$$$ week capital, week div, week currency, week total
	# 	for weekObj in week_return_data:
	# 		if(weekObj.stock_id == uHis.stock_id):
	# 			stock_list[i][5] = weekObj.capital_gain_local
	# 			stock_list[i][7] = weekObj.dividends_local
	# 			stock_list[i][9] = weekObj.currency_gain_aud
	# 			stock_list[i][11] = weekObj.total_gain_aud
				
	# 	#$$$ month capital, month div, month currency, month total
	# 	for weekObj in month_return_data:
	# 		if(weekObj.stock_id == uHis.stock_id):
	# 			stock_list[i][5] = weekObj.capital_gain_local
	# 			stock_list[i][7] = weekObj.dividends_local
	# 			stock_list[i][9] = weekObj.currency_gain_aud
	# 			stock_list[i][11] = weekObj.total_gain_aud
				
	# 	#$$$ three month capital, three month div, three month currency, three month total
	# 	for tmoObj in three_month_return_data:
	# 		if(tmoObj.stock_id == uHis.stock_id):
	# 			stock_list[i][5] = tmoObj.capital_gain_local
	# 			stock_list[i][7] = tmoObj.dividends_local
	# 			stock_list[i][9] = tmoObj.currency_gain_aud
	# 			stock_list[i][11] = tmoObj.total_gain_aud		


	# 	#$$$ year capital, year div, year currency, year total
	# 	for yearObj in year_return_data:
	# 		if(yearObj.stock_id == uHis.stock_id):
	# 			stock_list[i][5] = yearObj.capital_gain_local
	# 			stock_list[i][7] = yearObj.dividends_local
	# 			stock_list[i][9] = yearObj.currency_gain_aud
	# 			stock_list[i][11] = yearObj.total_gain_aud

	# 	#$$$ ytd capital, ytd div, ytd currency, ytd total
	# 	for ytd in ytd_return_data:
	# 		if(ytd.stock_id == uHis.stock_id):
	# 			stock_list[i][5] = ytd.capital_gain_local
	# 			stock_list[i][7] = ytd.dividends_local
	# 			stock_list[i][9] = ytd.currency_gain_aud
	# 			stock_list[i][11] = ytd.total_gain_aud


	# 	#$$$ financial capital, financial div, financial currency, financial total
	# 	for finObj in finyear_return_data:
	# 		if(finObj.stock_id == uHis.stock_id):
	# 			stock_list[i][5] = finObj.capital_gain_local
	# 			stock_list[i][7] = finObj.dividends_local
	# 			stock_list[i][9] = finObj.currency_gain_aud
	# 			stock_list[i][11] = finObj.total_gain_aud



	#getting the values for the table 
	date_today = datetime.date.today()

	return render_to_response('vues/portfolio.html', locals(), context_instance=RequestContext(request))

@login_required
def getportfolio(request, date_n):
	if(int(date_n) == 0):
		#all 
		portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date')
	elif(int(date_n) == 1):
		#1 week
		n_days = datetime.date.today() - timedelta(days=7)
		portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
	elif(int(date_n) == 2):
		#1 month
		n_days = datetime.date.today() - relativedelta(months=1)
		portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
	elif(int(date_n) == 3):
		#3 months 
		n_days = datetime.date.today() - relativedelta(months=2)
		portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
	elif(int(date_n) == 4):
		#1 year
		n_days = datetime.date.today() - relativedelta(years=1)
		portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
	elif(int(date_n) == 5):
		#Year to date
		n_days = date(date.today().year, 1, 1)
		portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
	elif(int(date_n) == 6):
		#Financial Year
		if( date.today().month >=7 ):
			#July onwards, get this current year's
			n_days = date(date.today().year, 7, 1)
			portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
		else:
			#last year's june
			n_days = date(date.today().year -1 , 7, 1)
			portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
	return render_to_response('vues/getportfolio.html', locals(), context_instance=RequestContext(request))

@login_required
def getportfolio_percent(request, date_n):
	if(int(date_n) == 0):
		#all 
		portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date')
	elif(int(date_n) == 1):
		#1 week
		n_days = datetime.date.today() - timedelta(days=7)
		portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
	elif(int(date_n) == 2):
		#1 month
		n_days = datetime.date.today() - relativedelta(months=1)
		portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
	elif(int(date_n) == 3):
		#3 months 
		n_days = datetime.date.today() - relativedelta(months=2)
		portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
	elif(int(date_n) == 4):
		#1 year
		n_days = datetime.date.today() - relativedelta(years=1)
		portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
	elif(int(date_n) == 5):
		#Year to date
		n_days = date(date.today().year, 1, 1)
		portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
	elif(int(date_n) == 6):
		#Financial Year
		if( date.today().month >=7 ):
			#July onwards, get this current year's
			n_days = date(date.today().year, 7, 1)
			portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
		else:
			#last year's june
			n_days = date(date.today().year -1 , 7, 1)
			portfolio_values = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('date').filter(Q(date__gte=n_days))
	return render_to_response('vues/getportfolio_percent.html', locals(), context_instance=RequestContext(request))

@login_required
def portfolio_allocations(request):
	return render_to_response('vues/portfolioallocations.html', locals(), context_instance=RequestContext(request))

@login_required
def getallocations_country(request):
	#get most recent portfolio value 
	try:
		portfolio_value = UserPortfolioValue.objects.filter(user_id=3).order_by('-date')[0]
	except:
			portfolio_value = 0

	user_stocks_list = UserStocksOrders.objects.filter(user_id=3).order_by('stock')
	stocks_list = Stocks.objects.filter(id__in=user_stocks_list.values('stock'))
	stocks_fin_list = StockFinancialData.objects.filter(stock_id__in=user_stocks_list.values('stock'))
	# stock_sum = 0
	# for stockFin in stocks_fin_list:
	# 	for userStockNum in user_stocks_list:
	# 		if(stockFin.stock_id == userStockNum.stock_id):
	# 			stock_sum = stock_sum + (stockFin.closing_price * userStockNum.quantity)

	return render_to_response('vues/getallocations_country.html', locals(), context_instance=RequestContext(request))

@login_required
def addfavourite(request,vue_name):
	vue_new = vue_name.replace("-", " ")
	vue = Vues.objects.get(name=vue_new)
	if (UserFavourites.objects.filter(user_id=request.user.id, vue_id=vue.id).exists()):
		UserFavourites.objects.filter(user_id=request.user.id,vue_id=vue.id).delete()
		return HttpResponseRedirect("/vues/"+vue_name) #change this to redirect to the correct vue	
	UserFavourites.objects.create(user_id=request.user.id,vue_id=vue.id)
	return HttpResponseRedirect("/vues/"+vue_name) #change this to redirect to the correct vue

@login_required
def removefavourite(request, vue_name):
	vue_name_space = vue_name.replace("-", " ")
	vue = Vues.objects.get(name=vue_name_space)
	UserFavourites.objects.filter(user_id=request.user.id, vue_id=vue.id).delete()
	return HttpResponseRedirect("/favourites/") 

@login_required
def orders(request):
	order_list = UserStocksOrders.objects.filter(user_id=request.user.id)
	vues_info = Vues.objects.filter(id__in = order_list.values('vue_id'))
	stocks_info = Stocks.objects.filter(id__in = order_list.values('stock_id'))
	
	orders_status = StocksOrdersStatus.objects.filter(order_id__in = order_list.values('id')).distinct()

	complete_orders = StocksOrdersStatus.objects.filter(
		                        order_id__in = order_list.values('id'),
		                        order_status="Filled").distinct()

	cancelled_orders = StocksOrdersStatus.objects.filter(
		                        order_id__in = order_list.values('id'),
		                        order_status="Cancelled").distinct()

	#pending_orders = order_list.exclude(id__in = complete_orders.values('order_id')).distinct()

	pending_orders = orders_status.exclude(order_id__in = complete_orders.values('order_id')).exclude(order_id__in = cancelled_orders.values('order_id')).distinct()
	return render_to_response('vues/orders.html', locals(), context_instance=RequestContext(request))

@login_required
def buy(request, vue_name):
	vue = Vues.objects.get(name=vue_name.replace("-", " "))
	user_info = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('-date')[0]
	stockinfos = VueStocks.objects.filter(vue_id = vue.id).values_list('stock_id', flat=True)
	stocks = Stocks.objects.filter(id__in = stockinfos).exclude(exchange_code_id__isnull=True)
	stockFinancials = StockFinancialData.objects.filter(stock_id__in = stocks)
	return render_to_response('vues/buy.html', locals(), context_instance=RequestContext(request)) 

@login_required
@csrf_exempt
def place_order(request):
	import json
	#	This function is called by AJAX in 'buy.js' on 'confirm' button

	if request.is_ajax():

		orders = json.loads(request.body)
		vue_id = int(orders['vue_id'])

		try:
			if p_order in globals():
				print 'p_order_exists: ',p_order
		except:
			print 'p_order does not exist'
			global p_order
			p_order = order_manager.OrderManager(int(request.user.id))
		finally:		
			print 'p_order:  ', p_order

		for single_order in orders['order_list']:

			stock_name = str(single_order['stock_ticket'])
			quantity = int(single_order['stock_quantity'])
			stock_id = int(single_order['stock_id'])
			stock_price = float(single_order['stock_price'])

			date_created = datetime.datetime.now()
			# save orders to DB-table 
			db_order = UserStocksOrders(user_id = int(request.user.id),
									  date_created = date_created,
									  stock_id = stock_id,
									  quantity = quantity,
									  price = stock_price,
									  vue_id = vue_id,
									  buy_sell = True)
			db_order.save()
			order_id = db_order.id #+ 23742010

			# initilize status of all order to 'PendingSubmit'
			db_order_status = StocksOrdersStatus(
			                  order_status = "PendingSubmit",
				              num_filled = 0,
				              num_remaining = quantity,
				              avg_fill_price = stock_price,
				              tws_id = 0,
				              last_fill_price = 0, 
				              date_updated = date_created)

			db_order_status.order = UserStocksOrders.objects.get(id = int(order_id)) # (int(order_id) - 23742010))
			db_order_status.save()

			# stock_name, quantity, 1, order_id, exchange_code(ib_code), currency_code(code)


			db_stock = Stocks.objects.get(id = stock_id)
			exchange_code =  db_stock.exchange_code.ib_code
			currency_code = db_stock.currency_code.code

			# make order for submission to TWS-API
			p_order.create_order(stock_name, quantity, 1, exchange_code, currency_code, 'BUY')
			p_order.place_order(order_id)

	return HttpResponse("OK")

@login_required
def transactions(request):
	try :
		portfolio_value = UserPortfolioValue.objects.filter(user_id=request.user.id).order_by('-date')[0]
	except:
		portfolio_value = 0
	return render_to_response('vues/transactions.html', locals(), context_instance=RequestContext(request))

@login_required 
def statements(request):
	return render_to_response('vues/statements.html', locals(), context_instance=RequestContext(request))

@login_required
def funding(request):
	return render_to_response('vues/funding.html', locals(), context_instance=RequestContext(request))
