from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import auth, messages
from django.contrib.auth import authenticate, login
from django.template import RequestContext
from vues.models import UserActivated
from django.core.mail import send_mail
from users.models import RegisterForm
import mandrill, os, binascii
from users.models import User, UserModel
from users.forms import ProfileForm

from vues.models import Vues, VueFinancialData

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

# register to the website
def register(request):
	if request.method == 'POST':	
		form = RegisterForm(request.POST)
		if form.is_valid():
			new_user = form.save()
			messages.info(request, "Thanks for registering. You are now logged in.")
			new_user = authenticate(username=request.POST['username'], password=request.POST['password1'])
			auth.login(request, new_user)
			random = binascii.b2a_hex(os.urandom(15))
			UserActivated.objects.create(user_id=request.user.id,isActivated=False,randomText=random)
			UserModel.objects.create(user_id=request.user.id,avatar='avatars/macrovue_small_logo.png',
			first_name=request.POST['first_name'],last_name=request.POST['last_name'],email=request.POST['email'])
			subject, from_email, to = 'Welcome to Macrovue', 'sid@macrovue.com.au', request.POST['email']

			html_content = render_to_string('mail.html', {'username':request.user.username}) # ...
			text_content = strip_tags(html_content) # this strips the html, so people will have the text as well.

			# create the email, and attach the HTML version as well.
			msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
			msg.attach_alternative(text_content, "text/html")
			msg.content_subtype = "html"
			msg.send()
	
			return HttpResponseRedirect("/existing_vues")
		else:
			return render(request, 'users/register.html', { 'form': form })

	return render(request, 'users/register.html', { 'form': form })

def register_view(request):
	form = RegisterForm(auto_id=True)
	return render(request, 'users/register.html', { 'form': form })

def username_check(request, username):
	num = User.objects.filter(username = username).count()
	return HttpResponse(num)

def email_check(request, email):
	num = User.objects.filter(email = email).count()
	return HttpResponse(num)

# login to the website
def login(request):
	username = request.POST.get('username', '')
	password = request.POST.get('password', '')
	user = auth.authenticate(username=username, password=password)
	if request.method == 'POST':
		if user is not None and user.is_active:
			# Correct password, and the user is marked "active"
			auth.login(request, user)
			# Redirect to a success page.
			return HttpResponseRedirect("/existing_vues")
		else:
			messages.error(request, "Invalid Username or Password")
			return render_to_response('users/login.html', locals(), context_instance=RequestContext(request))
	else:
		return render_to_response('users/login.html', locals(), context_instance=RequestContext(request))

# logout of the website
def logout(request):
	auth.logout(request)
	# Redirect to a success page.
	return HttpResponseRedirect("/index")
	
@login_required
def activate_account(request,activation_id):
	# grab this users specific id
	activatedInfo = UserActivated.objects.get(user_id=request.user.id)
	if (activation_id == activatedInfo.randomText and activatedInfo.isActivated == False):
		activatedInfo.isActivated = True;
		activatedInfo.save()
		return HttpResponse("Successfuly activated your account.")
	elif (activatedInfo.isActivated == True):
		return HttpResponse("Your account is already active.")
	else:
		return HttpResponse("You need to use the correct code in your email.")

def view_profile(request,username):
	if (User.objects.filter(username=username).exists()):
		profile = User.objects.get(username=username)
		profile_info = profile.get_profile()
		vues = Vues.objects.filter(user=profile,is_visible=True)
		vue_financial_data = VueFinancialData.objects.filter(vue__custom_vue__isnull=False,vue__user=profile).order_by('date')
		return render_to_response('users/profile.html', locals(), context_instance=RequestContext(request))
	else:
		return HttpResponseRedirect("/existing_vues")
		
@login_required
def profile_settings(request):
	context = RequestContext(request)
	profile = request.user.get_profile()
	if request.method == 'POST':
		form = ProfileForm(request.POST, request.FILES, instance=request.user.get_profile())
		if form.is_valid():
			profile = form.save(commit=False)
			profile.user = request.user # Set the user object here
			profile.save() # Now you can send it to DB
			return render_to_response('users/settings.html', locals(), context)
	else:
		form = ProfileForm(instance=request.user.get_profile())

	return render_to_response('users/settings.html', locals(), context)