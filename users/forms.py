from django import forms
from users.models import UserModel

class ProfileForm(forms.ModelForm):
	biography = forms.CharField(required=False, widget=forms.Textarea(attrs={'placeholder': 'Enter your biography here'}))
	facebook = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Enter your Facebook profile link here'}))
	twitter = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Enter your Twitter profile link here'}))
	linkedin = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Enter your Linkedin profile link here'}))
	avatar = forms.FileField(required=False,initial='images/macrovue_logo_small.png')
	first_name = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Enter your first name here'}))
	last_name = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Enter your last name here'}))
	email = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Enter your email here'}))
	

	class Meta:
		model = UserModel
		fields = ('first_name','last_name','email','biography','facebook','twitter','linkedin','avatar')