from django.db import models
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User
from django.conf import settings

class RegisterForm(UserCreationForm):
	# declare the fields you will show
	username = forms.CharField(label="Username")
	# first password field
	password1 = forms.CharField(label="Password")
	# confirm password field
	password2 = forms.CharField(label="Repeat your password")
	email = forms.EmailField(label = "Email Address")
	first_name = forms.CharField(label = "First Name")
	last_name = forms.CharField(label = "Last Name")

	# this sets the order of the fields
	class Meta:
		model = User
		fields = ("first_name", "last_name", "email", "username", "password1", "password2", )

	# this redefines the save function to include the fields you added
	def save(self, commit=True):
		user = super(RegisterForm, self).save(commit=False)
		user.email = self.cleaned_data["email"]
		user.first_name = self.cleaned_data["first_name"]
		user.last_name = self.cleaned_data["last_name"]
		if commit:
			user.save()
		return user
		
class UserModel(models.Model):  
	user = models.OneToOneField(User)
	avatar = models.ImageField(upload_to="avatars/",blank=False)
	biography = models.CharField(max_length=140, default='')
	facebook = models.CharField(max_length=300, default='')
	twitter = models.CharField(max_length=300, default='')
	linkedin = models.CharField(max_length=300, default='')
	first_name = models.CharField(max_length=300, default='',blank=False)
	last_name = models.CharField(max_length=300, default='',blank=False)
	email = models.CharField(max_length=300, default='',blank=False)
	
	class Meta:
		db_table = "user_model"
		verbose_name_plural = "UserModel"
		
	def get_avatar(self):
		if not self.avatar:
			return settings.STATIC_URL+"images/macrovue_small_logo.png"
		else:
			return settings.MEDIA_URL+str(self.avatar)