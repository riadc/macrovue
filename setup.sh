#!/bin/bash
# This script installs all packages required to run macrovue on the web. These packages includes ones required by python/django and zeroMQ
echo "Starting installation of Django requirnments .... [DON't run as root]"
sudo apt-get -y update
sudo apt-get -y install libmysqlclient-dev python-dev
sudo apt-get -y install python-setuptools python-pip
sudo pip install -r requirements.txt

echo "Starting Java JDK/Git Installation..."
sudo apt-get -y install openjdk-7-jdk git-core uuid-dev
sudo apt-get -y install autoconf automake libtool gcc make 
sudo apt-get -y install gsettings-desktop-schemas pkg-config
sudo apt-get -y install xvfb # to simulate X11 display

echo "Starting IbPy Installation..."
sudo git clone https://github.com/blampe/IbPy
cd IbPy
sudo python setup.py install

echo "Starting Apache2 Installation..."
sudo apt-get -y install apache2
sudo apt-get -y install libapache2-mod-wsgi

echo "Installation has been finished"
