#!/bin/bash

DATE=`date +%Y%m%d-%H%M`
log_dir=/tmp/tws_ib
if [ ! -d "$log_dir" ]; then
	mkdir /tmp/tws_ib
fi
LOGFILE=/tmp/tws_ib/${DATE}.log

#sudo xvfb-run -a java -cp jts.jar:total.2013.jar:IBController.jar -Xmx512M -XX:MaxPermSize=128M ibcontroller.IBGatewayController IBController.ini
nohup sudo xvfb-run -a java -cp jts.jar:total.2013.jar:IBController.jar -Xmx512M -XX:MaxPermSize=128M ibcontroller.IBGatewayController IBController.ini > ${LOGFILE} 2>&1 &
