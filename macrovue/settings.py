"""
Django settings for macrovue project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#=cd1r1k&k&&ehct0_o^e^#^#$sz_$+ne*(@%(f%cio@0lts)+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

# django
CORE_APPS = (
  # django admin
  'django.contrib.admin',
  'django.contrib.auth',
  'django.contrib.contenttypes',
  'django.contrib.sessions',
  'django.contrib.sites',
  'django.contrib.messages',
  'django.contrib.staticfiles',
  'django.contrib.admindocs',
  'django.contrib.comments',
  'django.contrib.humanize',
)

TEMPLATE_CONTEXT_PROCESSORS = (
  'django.contrib.messages.context_processors.messages',
  'django.contrib.auth.context_processors.auth',
  'django.core.context_processors.i18n',
  'django.core.context_processors.request',
  'zinnia.context_processors.version',  # Optional
  "django.core.context_processors.media",
)

EXTERNAL_APPS = (
  'django_extensions',
  'south',
  'mathfilters',
  #'debug_toolbar'
  # If you're using Django 1.7.x or later
  #'debug_toolbar.apps.DebugToolbarConfig',
  # If you're using Django 1.6.x or earlier
 # 'debug_toolbar',
)

LOCAL_APPS = (
  'home',
  'vues',
  'users',
  'tagging',
  'mptt',
  'zinnia',
)

# the order is important!
INSTALLED_APPS = CORE_APPS + LOCAL_APPS + EXTERNAL_APPS

MIDDLEWARE_CLASSES = (
  'django.contrib.sessions.middleware.SessionMiddleware',
  'django.middleware.common.CommonMiddleware',
  'django.middleware.csrf.CsrfViewMiddleware',
  'django.contrib.auth.middleware.AuthenticationMiddleware',
  'django.contrib.messages.middleware.MessageMiddleware',
  'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_HOST_USER = 'sid@macrovue.com.au'
EMAIL_HOST_PASSWORD = '0vns4KRYJHILIJeXKMkX8Q'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

ROOT_URLCONF = 'macrovue.urls'

WSGI_APPLICATION = 'macrovue.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
  'default': {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': 'mv_db',
    'USER': 'root',
    'PASSWORD': 'awiventures',
    'HOST': 'mv-db.ck2w2jitsnqg.ap-southeast-2.rds.amazonaws.com',
    'PORT': '3306',
  }
}


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 3


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '/home/ubuntu/macrovue.com/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR+'/media/'

TEMPLATE_DIRS = (
  os.path.join(os.path.dirname(__file__), '../templates').replace('\\','/')
)

MAILCHIMP_API_KEY = "47c82dfd1eb7999ead049245cd64bdbb-us8"
MAILCHIMP_LIST_NAME = "Macrovue Subscriber List"

# FOR THE USERS PROFILE
AUTH_PROFILE_MODULE = 'users.UserModel'

# Additional locations of static files
STATICFILES_DIRS = (
  '%s/static' % BASE_DIR,
)

if os.environ.get('DEVELOPMENT', None):
  from settings_dev import *

LOGIN_URL = '/index'

DISQUS_SHORTNAME = 'macrovuestaging'
