from django.conf.urls import patterns, include, url

from django.contrib import admin
from home import views
from django.conf import settings
from django.conf.urls.static import static

admin.autodiscover()

urlpatterns = patterns('',
    # make this the home page
    url(r'^$', 'vues.views.index', name='index'),
	#if not logged in, index page
    url(r'^index/$', 'vues.views.index', name='index'),
    url(r'^index$', 'vues.views.index', name='index'),
    # for comments
    url(r'^comments/', include('django.contrib.comments.urls')),
    #display the vue
    url(r'^vues/(?P<vue_name>[-\w ]+)/$', 'vues.views.show', name='single_vue'),
	# customise the vue
    url(r'^vues/(?P<vue_name>[-\w ]+)/customise$', 'vues.views.customise', name='customise_vue'),
	# create a vue
    url(r'^create_vue$', 'vues.views.create_vue', name='create_vue'),	
	# create a vue
    url(r'^save_new_vue$', 'vues.views.save_new_vue', name='save_new_vue'),	
	# save the custom vue
    url(r'^save_custom/$', 'vues.views.save_custom', name='save_custom'),	
	# add the vue as a favourite
	url(r'^vues/addfavourite/(?P<vue_name>[-\w ]+)$', 'vues.views.addfavourite', name='add_favourite'),
	# add the vue as a favourite
	url(r'^vues/removefavourite/(?P<vue_name>[-\w ]+)$', 'vues.views.removefavourite', name='remove_favourite'),
	# submit a rating for a vue
	url(r'^vues/submit_rating/(?P<vue_id>\d+)/(?P<rating>\d+)$', 'vues.views.submit_rating', name='submit_rating'),
	# get stocks for that vue
    url(r'^getstocks/(?P<ticker_name>[-\w ]+)/(?P<index>[-\w ]+)$', 'vues.views.getstocks', name='get_stocks'),
    # get their financials
    url(r'^getstockfinancials/(?P<ticker_id>\d+)$', 'vues.views.getstockfinancials', name='get_stock_financials'),
	# get the vues
    url(r'^getvues/(?P<menu>\d+)$', 'vues.views.getvues', name='get_vues'),
    # view profiles
    url(r'^profile/(?P<username>[-\w ]+)$', 'users.views.view_profile', name='view_profile'),
    # settings
    url(r'^settings$', 'users.views.profile_settings', name='profile_settings'),
    # get vue returns
    url(r'^getvuereturns/(?P<vue>[-\w ]+)$', 'vues.views.getvuereturns', name='get_vue_returns'),
	# get a list of vues
    url(r'^existing_vues$', 'vues.views.list', name='existing_vues'),
    # get a list of users vues
    url(r'^users_vues$', 'vues.views.users_vues', name='users_vues'),
	# the blog link
    url(r'^blog/', include('zinnia.urls')),
	# the generic comments
    url(r'^comments/', include('django.contrib.comments.urls')),
	# subscribe to the mailing list
    url(r'^subscribe/$', 'vues.views.subscribe', name='subscribe'),
	# admin cp
    url(r'^admin/', include(admin.site.urls)),
	# register to the site
    url(r'^register/$', 'users.views.register_view', name='register'),
    url(r'^register-submit/$', 'users.views.register', name='register_submit'),
    #does username or email exist
    url(r'^usernamecheck/(?P<username>[-\w ]+)$', 'users.views.username_check', name='username_check'),
    url(r'^emailcheck/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})$', 'users.views.email_check', name='email_check'),
    # activate account
    url(r'^activate/(?P<activation_id>[-\w ]+)$', 'users.views.activate_account', name='activate'),
	# login
    url(r'^login/$', 'users.views.login', name='login'),
    url(r'^login/$', 'users.views.login', name='auth_login'),
	# logout
    url(r'^logout/$', 'users.views.logout', name='logout'),
    # about
    url(r'^about/$', 'home.views.about', name='about'),
    # faq
    url(r'^faq/$', 'home.views.faq', name='faq'),
    # my favourites page under my accounts 
    url(r'^favourites/$', 'vues.views.favourites', name='favourites'),
    # custom vues
    url(r'^customvues/$', 'vues.views.customvues', name='customvues'),
    # portfolio 
    url(r'^portfolio/$', 'vues.views.portfolio', name='portfolio'),
    #getting the portfolio values for the graph json
    url(r'^getportfolio/(?P<date_n>\d+)$', 'vues.views.getportfolio', name='getportfolio'),
    url(r'^getportfolio_percent/(?P<date_n>\d+)$', 'vues.views.getportfolio_percent', name='getportfolio_percent'),
    #portfolio-returns 
    url(r'^portfolio-allocations/$', 'vues.views.portfolio_allocations', name='portfolio_allocations'),
    #get-allocations-country
    url(r'^getallocations-country/$', 'vues.views.getallocations_country', name='getallocations_country'),
    #orders - checking status 
    url(r'^orders/$', 'vues.views.orders', name='orders'),
    #buy - purchase the stocks in that vue
    url(r'^buy/(?P<vue_name>[-\w ]+)/$', 'vues.views.buy', name='buy'),
    #place_order - sends orders to the zeroMQ
    url(r'^place_order/$', 'vues.views.place_order', name='place_order'),
    #view transactions
    url(r'^transactions/$', 'vues.views.transactions', name='transacitons'),
    #view statements
    url(r'^statements/$', 'vues.views.statements', name='statements'),
    #funding
    url(r'^funding/$', 'vues.views.funding', name='funding')
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)